import django_filters

from src.apps.core.models import Order


class OrderApiFilter(django_filters.FilterSet):
    start = django_filters.DateFilter(field_name="created_at", lookup_expr='gte')
    end = django_filters.DateFilter(field_name="created_at", lookup_expr='lte')

    class Meta:
        model = Order
        fields = ['start', 'end']
