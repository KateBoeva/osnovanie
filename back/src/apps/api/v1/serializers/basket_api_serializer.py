from rest_framework import serializers

from src.apps.core.models import Basket


class BasketApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Basket
        fields = '__all__'
