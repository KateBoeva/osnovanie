from rest_framework import serializers

from src.apps.core.models import Expense


class ExpenseApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expense
        fields = '__all__'
