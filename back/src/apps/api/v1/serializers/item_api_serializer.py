from rest_framework import serializers

from src.apps.api.v1.serializers.item_recipe_api_serializer import ItemRecipeApiSerializer
from src.apps.api.v1.serializers.menu_api_serializer import MenuApiSerializer
from src.apps.core.models import Item


class ItemApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ['id', 'name', 'description', 'item_category', 'cost', 'need_show_default_option_value']

    def to_representation(self, instance):
        response = super().to_representation(instance)

        # 0 - visible, 1 - hidden, 2 - deleted
        response['status'] = 2 if instance.is_deleted else 1 if instance.is_hidden else 0
        response['self_price'] = round(instance.self_price or sum([i.count * i.default_product.average_cost for i in instance.recipes.all()]), 2)
        response['ingredients'] = ItemRecipeApiSerializer(instance.recipes, many=True).data
        response['menu_item'] = MenuApiSerializer(instance.menu_items.first()).data
        response['name'] = response['menu_item']['name'] or instance.name

        return response
