from rest_framework import serializers

from src.apps.core.models import ItemCategory


class ItemCategoryApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = '__all__'
