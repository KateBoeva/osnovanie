from django.db.models import Q
from rest_framework import serializers

from src.apps.api.v1.serializers.item_api_serializer import ItemApiSerializer
from src.apps.core.models import ItemCategory


class ItemCategoryGoodsApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)

        # отбираем актуальные товары и сортируем по указанному порядку
        filtered_items = list(instance.items.filter(menu_items__is_hidden=False, is_deleted=False))
        sorted_items = sorted(filtered_items, key=lambda d: d.menu_items.first().order_num)

        response['goods'] = ItemApiSerializer(sorted_items, many=True).data
        return response
