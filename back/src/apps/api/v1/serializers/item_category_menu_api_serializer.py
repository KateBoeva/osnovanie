from rest_framework import serializers

from src.apps.api.v1.serializers.item_api_serializer import ItemApiSerializer
from src.apps.core.models import ItemCategory


class ItemCategoryMenuApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['goods'] = ItemApiSerializer(instance.items, many=True).data
        return response
