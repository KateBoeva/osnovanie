from rest_framework import serializers


class KeyValueSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()

    # def to_representation(self, instance):
    #     response = super().to_representation(instance)
    #     return {response['id']: response['name']}

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass
