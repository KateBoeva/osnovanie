from rest_framework import serializers

from src.apps.core.models import Measure


class MeasureApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Measure
        fields = '__all__'
