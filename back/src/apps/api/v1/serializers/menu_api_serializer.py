from rest_framework import serializers

from src.apps.api.v1.serializers.key_value_serializer import KeyValueSerializer
from src.apps.core.models import MenuItem


class MenuApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuItem
        exclude = ['item']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['measure'] = KeyValueSerializer(instance.measure).data
        return response
