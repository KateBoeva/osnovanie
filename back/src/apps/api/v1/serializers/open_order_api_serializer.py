from rest_framework import serializers

from src.apps.core.models import OpenOrder


class OpenOrderApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = OpenOrder
        fields = '__all__'
