from rest_framework import serializers

from src.apps.api.v1.serializers.order_content_api_serializer import OrderContentApiSerializer
from src.apps.core.models import Order


class OrderApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['order_content'] = OrderContentApiSerializer(instance.order_content, many=True).data
        return response
