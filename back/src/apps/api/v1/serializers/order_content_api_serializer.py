from rest_framework import serializers

from src.apps.core.models import OrderContent


class OrderContentApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderContent
        fields = ['id', 'count']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['name'] = instance.modification.item.name
        return response
