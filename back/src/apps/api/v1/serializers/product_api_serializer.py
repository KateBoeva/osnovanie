from rest_framework import serializers

from src.apps.api.v1.serializers.key_value_serializer import KeyValueSerializer
from src.apps.api.v1.serializers.product_recipe_api_serializer import ProductRecipeApiSerializer
from src.apps.core.models import Product


class ProductApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['tag'] = KeyValueSerializer(instance.tag).data
        response['measure'] = KeyValueSerializer(instance.measure).data
        response['ingredients'] = ProductRecipeApiSerializer(instance.product_recipes, many=True).data or None
        return response
