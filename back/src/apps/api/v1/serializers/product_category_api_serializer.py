from rest_framework import serializers

from src.apps.core.models import ProductCategory


class ProductCategoryApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = '__all__'
