from rest_framework import serializers

from src.apps.core.models import ProductLog


class ProductLogApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductLog
        fields = '__all__'
