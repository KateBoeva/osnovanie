from rest_framework import serializers

from src.apps.core.models import ProductRecipe


class ProductRecipeApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductRecipe
        fields = ['id', 'count']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['product'] = instance.default_product.name
        return response
