from rest_framework import serializers

from src.apps.core.models import RecipeProductTag


class RecipeProductTagApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeProductTag
        fields = '__all__'
