from django.urls import path
from rest_framework.routers import SimpleRouter

from src.apps.api.v1.views.analytics_main_viewset import AnalyticsMainView
from src.apps.api.v1.views.baskets_viewset import BasketsViewSet
from src.apps.api.v1.views.expenses_viewset import ExpensesViewSet
from src.apps.api.v1.views.item_categories_viewset import ItemCategoriesViewSet
from src.apps.api.v1.views.item_recipes_viewset import ItemRecipesViewSet
from src.apps.api.v1.views.items_viewset import ItemsViewSet
from src.apps.api.v1.views.measures_viewset import MeasuresViewSet
from src.apps.api.v1.views.menu_api_viewset import MenuApiViewSet
from src.apps.api.v1.views.open_orders_viewset import OpenOrdersViewSet
from src.apps.api.v1.views.order_api_viewset import OrderApiViewSet
from src.apps.api.v1.views.product_categories_viewset import ProductCategoriesViewSet
from src.apps.api.v1.views.product_logs_viewset import ProductLogsViewSet
from src.apps.api.v1.views.products_viewset import ProductsViewSet
from src.apps.api.v1.views.recipe_product_tags_viewset import RecipeProductTagsViewSet

app_name = "api"

router = SimpleRouter()

router.register("baskets", BasketsViewSet, basename="baskets")
router.register("products", ProductsViewSet, basename="products")
router.register("products_categories", ProductCategoriesViewSet, basename="products_categories")
router.register("products_logs", ProductLogsViewSet, basename="products_logs")
router.register("recipe_product_tags", RecipeProductTagsViewSet, basename="recipe_product_tags")
router.register("item_categories", ItemCategoriesViewSet, basename="item_categories")
router.register("item_recipes", ItemRecipesViewSet, basename="item_recipes")
router.register("goods", ItemsViewSet, basename="goods")
router.register("measures", MeasuresViewSet, basename="measures")
router.register("open_orders", OpenOrdersViewSet, basename="open_orders")
router.register("orders", OrderApiViewSet, basename="orders")
router.register("expenses", ExpensesViewSet, basename="expenses")
router.register("menu", MenuApiViewSet, basename="menu")

urlpatterns = [
    path('analytics/main', AnalyticsMainView.as_view()),
]

urlpatterns += router.urls
