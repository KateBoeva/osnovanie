import datetime

from django.db.models import Sum, F
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.views import APIView

from src.apps.core.models import Order


class AnalyticsMainView(APIView):
    @swagger_auto_schema(
        operation_summary="Общая аналитика",
        manual_parameters=[
            openapi.Parameter('date', openapi.IN_QUERY, type=openapi.TYPE_STRING)
        ]
    )
    def get(self, request):
        query = request.GET.get('date')
        day = datetime.date.today() if not query else datetime.datetime.strptime(query, "%Y-%m-%d").date()

        queryset = Order.objects.filter(created_at__month=day.month, created_at__year=day.year)
        month_self_price = sum([i.self_price for i in queryset])
        total_month = queryset.aggregate(total_month=Sum(F('total')))['total_month']

        total_month = int(total_month or 0)
        margin_per_month = total_month - int(month_self_price)

        return Response({
            'revenue_per_month': total_month,
            'bills_per_month': len(queryset),
            'margin_per_month': margin_per_month,
            'marginality_per_month': round(margin_per_month / total_month * 100),
        })
