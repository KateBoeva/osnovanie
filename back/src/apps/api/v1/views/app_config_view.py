from rest_framework.response import Response
from rest_framework.views import APIView

from src.apps.api.v1.serializers.key_value_serializer import KeyValueSerializer
from src.apps.common.enums.product_type_enum import ProductType
from src.apps.common.enums.status_enum import Status
from src.apps.core.models import Measure, ProductCategory, ItemCategory


class AppConfigView(APIView):
    def get(self, request):
        return Response({
            'measures': KeyValueSerializer(Measure.objects.all(), many=True).data,
            'product_types': ProductType.choices(),
            'product_categories': KeyValueSerializer(ProductCategory.objects.all(), many=True).data,
            'good_categories': KeyValueSerializer(ItemCategory.objects.all(), many=True).data,
            'order_statuses': Status.choices(),
        })
