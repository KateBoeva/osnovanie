from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.basket_api_serializer import BasketApiSerializer
from src.apps.core.models import Basket


class BasketsViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = Basket.objects.all()
    serializer_class = BasketApiSerializer
