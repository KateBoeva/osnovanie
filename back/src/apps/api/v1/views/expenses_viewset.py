from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.expense_api_serializer import ExpenseApiSerializer
from src.apps.core.models import Expense


class ExpensesViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = Expense.objects.all()
    serializer_class = ExpenseApiSerializer
