from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.item_category_api_serializer import ItemCategoryApiSerializer
from src.apps.api.v1.serializers.item_category_goods_api_serializer import ItemCategoryGoodsApiSerializer
from src.apps.core.models import ItemCategory


class ItemCategoriesViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = ItemCategory.objects.all()
    serializer_class = ItemCategoryGoodsApiSerializer
