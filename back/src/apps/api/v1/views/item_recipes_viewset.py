from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.item_recipe_api_serializer import ItemRecipeApiSerializer
from src.apps.core.models import ItemRecipe


class ItemRecipesViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = ItemRecipe.objects.all()
    serializer_class = ItemRecipeApiSerializer
