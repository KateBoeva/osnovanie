from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.item_api_serializer import ItemApiSerializer
from src.apps.core.models import Item


class ItemsViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemApiSerializer
