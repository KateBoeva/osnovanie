from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.measure_api_serializer import MeasureApiSerializer
from src.apps.core.models import Measure


class MeasuresViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = Measure.objects.all()
    serializer_class = MeasureApiSerializer
