from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.open_order_api_serializer import OpenOrderApiSerializer
from src.apps.core.models import OpenOrder


class OpenOrdersViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = OpenOrder.objects.all()
    serializer_class = OpenOrderApiSerializer
