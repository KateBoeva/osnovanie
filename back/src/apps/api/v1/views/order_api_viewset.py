from django.utils import timezone
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.filters.order_api_filter import OrderApiFilter
from src.apps.api.v1.serializers.order_api_serializer import OrderApiSerializer
from src.apps.core.models import Order


class OrderApiViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderApiSerializer
    filterset_class = OrderApiFilter

    def get_queryset(self):
        queryset = super().get_queryset()
        is_start = 'start' in self.request.GET.keys()
        is_end = 'end' in self.request.GET.keys()
        if not is_start and not is_end:
            return queryset.filter(created_at__date=timezone.localtime(timezone.now()).date())

        return queryset
