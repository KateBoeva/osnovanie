from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.product_category_api_serializer import ProductCategoryApiSerializer
from src.apps.core.models import ProductCategory


class ProductCategoriesViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategoryApiSerializer
