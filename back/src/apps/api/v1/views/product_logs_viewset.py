from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.product_log_api_serializer import ProductLogApiSerializer
from src.apps.core.models import ProductLog


class ProductLogsViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = ProductLog.objects.all()
    serializer_class = ProductLogApiSerializer
