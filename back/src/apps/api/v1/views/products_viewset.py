from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.product_api_serializer import ProductApiSerializer
from src.apps.core.models import Product


class ProductsViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductApiSerializer
