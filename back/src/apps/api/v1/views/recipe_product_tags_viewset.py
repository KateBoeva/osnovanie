from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from src.apps.api.v1.serializers.recipe_product_tag_api_serializer import RecipeProductTagApiSerializer
from src.apps.core.models import RecipeProductTag


class RecipeProductTagsViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = RecipeProductTag.objects.all()
    serializer_class = RecipeProductTagApiSerializer
