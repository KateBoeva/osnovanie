import enum
from enum import IntEnum


@enum.unique
class Action(IntEnum):
    WITHDRAWAL = 1
    ADDING = 2
    SELLING = 3

    SALE = 4  # продажа
    SUPPLY = 5  # поставка
    OVERDUE = 6  # просроченное
    BRAKERAGE = 7  # бракераж
    SPOILED = 8  # испортили - в результате готовки (упало, разбилось, и тд)
    STAFF = 9  # стафф обед
    REVISION = 10  # инвентаризация

    SETTING = 11  # настройка
    DEVELOPMENT = 12  # разработка

    BILLET = 13  # заготовки
    COMPLIMENT = 14  # подарок

    @classmethod
    def choices(cls):
        return (
            # (cls.WITHDRAWAL.value, 'Списание'),
            (cls.ADDING.value, 'Добавление'),
            (cls.SELLING.value, 'Продажа'),

            (cls.OVERDUE.value, 'Просроченное'),
            (cls.BRAKERAGE.value, 'Бракераж'),
            (cls.SPOILED.value, 'Испортили'),
            (cls.STAFF.value, 'Стафф'),
            (cls.REVISION.value, 'Инвентаризация'),

            (cls.SETTING.value, 'Настройка'),
            (cls.DEVELOPMENT.value, 'Разработка'),
            (cls.BILLET.value, 'Заготовка'),

            (cls.COMPLIMENT.value, 'Комплимент'),
        )

    @classmethod
    def withdrawals_dict(cls):
        return {
            # cls.WITHDRAWAL.value: 'Списание',
            # cls.ADDING.value: 'Добавление',
            # cls.SELLING.value: 'Продажа',

            cls.OVERDUE.value: 'Просроченное',
            cls.BRAKERAGE.value: 'Бракераж',
            cls.SPOILED.value: 'Испортили',
            cls.STAFF.value: 'Стафф',
            cls.REVISION.value: 'Инвентаризация',

            cls.SETTING.value: 'Настройка',
            cls.DEVELOPMENT.value: 'Разработка',
            # cls.BILLET.value: 'Заготовка',

            cls.COMPLIMENT.value: 'Комплимент',
        }

    @classmethod
    def reason_list(cls):
        return (
            (cls.OVERDUE.value, 'Просроченное'),
            (cls.BRAKERAGE.value, 'Бракераж'),
            (cls.SPOILED.value, 'Испортили'),
            (cls.STAFF.value, 'Стафф'),
            # (cls.REVISION.value, 'Инвентаризация'),
            #
            # (cls.SETTING.value, 'Настройка'),
            # (cls.DEVELOPMENT.value, 'Разработка'),

            (cls.COMPLIMENT.value, 'Комплимент'),
        )

    @classmethod
    def product_reason_list(cls):
        return (
            (cls.OVERDUE.value, 'Просроченное'),
            (cls.BRAKERAGE.value, 'Бракераж'),
            (cls.SPOILED.value, 'Испортили'),
            (cls.STAFF.value, 'Стафф'),
            (cls.REVISION.value, 'Инвентаризация'),

            (cls.SETTING.value, 'Настройка'),
            (cls.DEVELOPMENT.value, 'Разработка'),
        )

    @classmethod
    def reason_dict(cls):
        return {
            cls.WITHDRAWAL.value: 'Списание',
            cls.ADDING.value: 'Добавление',
            cls.SELLING.value: 'Продажа',

            cls.OVERDUE.value: 'Просроченное',
            cls.BRAKERAGE.value: 'Бракераж',
            cls.SPOILED.value: 'Испортили',
            cls.STAFF.value: 'Стафф',
            cls.REVISION.value: 'Инвентаризация',

            cls.SETTING.value: 'Настройка',
            cls.DEVELOPMENT.value: 'Разработка',
            cls.BILLET.value: 'Заготовка',

            cls.COMPLIMENT.value: 'Комплимент',
        }
