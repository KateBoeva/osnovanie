import enum
from enum import IntEnum


@enum.unique
class ExpenseType(IntEnum):
    CONSUMABLE = 1
    PRODUCTS = 2
    STAFF = 3
    CUSTOM = 4

    @classmethod
    def choices(cls):
        return (
            (cls.PRODUCTS.value, 'Сырьё'),
            (cls.CONSUMABLE.value, 'Расходники'),
            (cls.STAFF.value, 'Служебка'),
        )

    @classmethod
    def supply_choices(cls):
        return (
            (cls.PRODUCTS.value, 'Продукты'),
            (cls.CUSTOM.value, 'Другое'),
        )

    @classmethod
    def expenses_choices(cls):
        return (
            (cls.CONSUMABLE.value, 'Расходники'),
            (cls.STAFF.value, 'Служебка'),
        )

    @classmethod
    def dict(cls):
        return {
            cls.PRODUCTS.value: 'Сырьё',
            cls.CUSTOM.value: 'Сырьё (остальное)',
            cls.CONSUMABLE.value: 'Расходники',
            cls.STAFF.value: 'Служебка',
        }
