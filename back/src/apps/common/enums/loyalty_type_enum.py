import enum
from enum import IntEnum


@enum.unique
class LoyaltyType(IntEnum):
    CASHBACK = 1
    SALE = 2

    @classmethod
    def choices(cls):
        return (
            (cls.CASHBACK.value, 'Кэшбэк'),
            (cls.SALE.value, 'Скидка'),
        )
