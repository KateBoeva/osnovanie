import enum
from enum import IntEnum


@enum.unique
class ProductType(IntEnum):
    PRODUCT = 1  # обычный продукт
    BILLET = 2  # заготовка

    @classmethod
    def choices(cls):
        return (
            (cls.PRODUCT.value, 'Продукт'),
            (cls.BILLET.value, 'Заготовка'),
        )

    @classmethod
    def dict(cls):
        return {
            cls.PRODUCT.value: 'Продукт',
            cls.BILLET.value: 'Заготовка',
        }

    @classmethod
    def serialized(cls, type_num):
        return {
            "id": type_num,
            "name": cls.dict()[type_num]
        }
