import enum
from enum import IntEnum


@enum.unique
class Status(IntEnum):
    PAID = 1
    CANCELLED = 2
    WASTED = 3

    @classmethod
    def choices(cls):
        return (
            (cls.PAID.value, 'Оплачен'),
            (cls.CANCELLED.value, 'Отменён'),
            (cls.WASTED.value, 'Отменён с потерей продуктов'),
        )
