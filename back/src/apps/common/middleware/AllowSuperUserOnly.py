from django.http import HttpResponseRedirect
from django.utils.deprecation import MiddlewareMixin


class AllowSuperUserOnly(MiddlewareMixin):
    only_superuser = ['/admin/', '/products', '/expenses', '/products_log', '/swagger']

    def process_request(self, request):
        for url in self.only_superuser:
            if request.path.startswith(url) and not request.user.is_superuser:
                return HttpResponseRedirect(redirect_to='/')

        return None
