from django.contrib import admin

from src.apps.core.models import Item, Order, Product, Expense, ProductLog, ProductCategory, OpenOrder, Guest, MenuItem
from src.apps.core.models import RecipePositionProduct, Modification, OrderContent, DelayedSupplyItem
from src.apps.core.models import ItemCategory, ItemRecipe, RecipeProductTag, ProductProvider

from src.apps.core.models.product_recipe import ProductRecipe

admin.site.register(ItemCategory)


class RecipePositionProductAdmin(admin.TabularInline):
    model = RecipePositionProduct
    extra = 0


class ItemRecipeInlineAdmin(admin.TabularInline):
    model = ItemRecipe
    extra = 0


class MenuItemInlineAdmin(admin.TabularInline):
    model = MenuItem
    extra = 0


@admin.register(ItemRecipe)
class ItemRecipeAdmin(admin.ModelAdmin):
    inlines = [RecipePositionProductAdmin]


class ProductRecipeAdmin(admin.TabularInline):
    model = ProductRecipe
    fk_name = 'billet_product'


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    inlines = [ItemRecipeInlineAdmin, MenuItemInlineAdmin, ]
    save_as = True


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [ProductRecipeAdmin]
    readonly_fields = ['count']
    save_as = True


class OrderContentInlineAdmin(admin.TabularInline):
    model = OrderContent
    extra = 0


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    readonly_fields = ['status', 'user', 'comment', 'closed_at']
    inlines = [OrderContentInlineAdmin]


@admin.register(ProductLog)
class ProductLogAdmin(admin.ModelAdmin):
    readonly_fields = ['name', 'volume', 'cost', 'action', 'reason', 'created_at']


class DelayedSupplyItemInlineAdmin(admin.TabularInline):
    model = DelayedSupplyItem
    extra = 0


@admin.register(Expense)
class ProductLogAdmin(admin.ModelAdmin):
    inlines = [DelayedSupplyItemInlineAdmin]


@admin.register(Modification)
class ModificationAdmin(admin.ModelAdmin):
    save_as = True


admin.site.register(ProductRecipe)
admin.site.register(ProductProvider)
admin.site.register(RecipeProductTag)
admin.site.register(ProductCategory)
admin.site.register(OpenOrder)
admin.site.register(Guest)
