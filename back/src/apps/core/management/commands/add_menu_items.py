from django.core.management.base import BaseCommand

from src.apps.core.models import Item, MenuItem, ItemCategory


class Command(BaseCommand):
    def handle(self, *args, **options):
        categories = ItemCategory.objects.all()
        for category in categories:
            order_num = 1
            for item in category.items.all():
                MenuItem.objects.create(
                    item=item,
                    order_num=order_num,
                    img=None
                )
                order_num += 1

        print("all done.")
