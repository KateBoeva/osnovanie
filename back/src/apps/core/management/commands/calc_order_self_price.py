# from django.core.management.base import BaseCommand
#
# from src.apps.core.models import Order
#
#
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         for order in Order.objects.all():
#             self_price = 0
#             for c_item in order.configured_items.all():
#                 if c_item.item.self_price:
#                     self_price += c_item.item.self_price
#                 else:
#                     for recipe in c_item.item.recipes.all():
#                         set_product_tags = [i.product.tag for i in c_item.settings.all()]
#                         if recipe.default_product.tag not in set_product_tags:
#                             count = recipe.count * c_item.count
#                             self_price += count * recipe.default_product.average_cost
#                     for setting in c_item.settings.all():
#                         count = c_item.item.recipes.filter(product_tag=setting.tag).first().count * c_item.count
#                         self_price += count * setting.product.average_cost
#             order.self_price = self_price
#             order.save()
#             print("id: {}, total: {}, self_price: {}".format(order.id, order.total, order.self_price))
#         print("all done.")
