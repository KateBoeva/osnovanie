# from django.core.management.base import BaseCommand
#
# from src.apps.common.enums.action_enum import Action
# from src.apps.core.models import ProductLog
#
#
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         actions = {
#             "служебка": Action.STAFF,
#             "дане": Action.STAFF,
#             "артему": Action.STAFF,
#             "кати": Action.STAFF,
#             "кате": Action.STAFF,
#             "сабине": Action.STAFF,
#             "алине": Action.STAFF,
#             "настройка": Action.SETTING,
#             "техническое": Action.SETTING,
#             "хз": Action.REVISION,
#             "жарим": Action.REVISION,
#             "инвент": Action.REVISION,
#             "?": Action.REVISION,
#             "стафф": Action.STAFF,
#             "стаф": Action.STAFF,
#             "обед": Action.STAFF,
#             "разработка": Action.DEVELOPMENT,
#             "тренировка": Action.DEVELOPMENT,
#             "разработки": Action.DEVELOPMENT,
#             "проработка": Action.DEVELOPMENT,
#             "вечер": Action.OVERDUE,
#             "шняга": Action.OVERDUE,
#             "списание": Action.OVERDUE,
#             "шкурки": Action.OVERDUE,
#             "порча": Action.OVERDUE,
#             "остаток": Action.OVERDUE,
#             "сдох": Action.OVERDUE,
#             "срок": Action.OVERDUE,
#             "скисло": Action.OVERDUE,
#             "разбито": Action.SPOILED,
#             "треснуло": Action.SPOILED,
#             "испортился": Action.SPOILED,
#             "испортилась": Action.SPOILED,
#             "испортили": Action.SPOILED,
#             "проеб": Action.SPOILED,
#             "брак": Action.SPOILED,
#             "рисунке": Action.SPOILED,
#             "сгорел": Action.SPOILED,
#             "бракераж": Action.BRAKERAGE,
#             "на пробу": Action.BRAKERAGE,
#             "отходы": Action.OVERDUE,
#             "камплэмэнт": Action.COMPLIMENT,
#             "комплимент": Action.COMPLIMENT,
#             "передалал": Action.SPOILED,
#             "ботаники": Action.COMPLIMENT,
#             "ожидание": Action.COMPLIMENT,
#             "собес": Action.COMPLIMENT,
#             "анвару": Action.COMPLIMENT,
#             "вода": Action.COMPLIMENT,
#             "умир": Action.OVERDUE,
#             "домой": Action.OVERDUE,
#             "плохой": Action.OVERDUE,
#             "плохое": Action.OVERDUE,
#             "пллохой": Action.OVERDUE,
#             "списать": Action.STAFF,
#             "фото": Action.DEVELOPMENT,
#             "инсты": Action.DEVELOPMENT
#         }
#
#         for log in ProductLog.objects.all():
#             if log.reason:
#                 reason = log.reason.lower()
#                 for action in actions:
#                     if action in reason:
#                         log.action = actions[action]
#             else:
#                 log.action = Action.REVISION
#             log.save()
#
#         for log in ProductLog.objects.filter(action=Action.WITHDRAWAL):
#             log.action = Action.STAFF
#             log.save()
#
#         print("all done.")
