# import time
#
# from django.core.management.base import BaseCommand
#
# from src.apps.core.models import ConfiguredItem, Modification, Item, RecipePositionProduct, OrderContent
#
#
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         tm = time.time()
#
#         print("Creating all settings in new table 'item_recipe_setting'...")
#         for i in Item.objects.all():
#             only_recipe = []
#             ingredients = {}
#             for r in i.recipes.all():
#                 rpp = RecipePositionProduct.objects.create(
#                     extra_cost=0,
#                     is_default=True,
#                     item_recipe=r,
#                     product=r.default_product
#                 )
#                 if r.product_tag:
#                     if r.product_tag.id in ingredients.keys():
#                         ingredients[r.product_tag.id] += [rpp]
#                     else:
#                         ingredients.update({r.product_tag.id: [rpp]})
#                 else:
#                     only_recipe += [rpp]
#
#             for s in i.settings.all():
#                 rpp = RecipePositionProduct.objects.create(
#                     extra_cost=s.extra_cost,
#                     item_recipe=i.recipes.filter(product_tag=s.tag).first(),
#                     product=s.product
#                 )
#
#                 if s.tag:
#                     if s.tag.id in ingredients.keys():
#                         ingredients[s.tag.id] += [rpp]
#                     else:
#                         ingredients.update({s.tag.id: [rpp]})
#
#             i_keys = list(ingredients.keys())
#             tag_count = len(i_keys)
#             if tag_count == 0:
#                 # если это просто рецепт блюда, у которого нет вариаций
#                 # добавляем каждый ингредиент в настройку как is_default (без тега)
#                 m = Modification.objects.create(item=i)
#
#                 if only_recipe:
#                     for rpp in only_recipe:
#                         m.settings.add(rpp)
#
#                     m.save()
#             elif tag_count == 1:
#                 first = ingredients[i_keys[0]]
#                 for f in first:
#                     m = Modification.objects.create(item=i)
#                     m.settings.add(f)
#                     if only_recipe:
#                         for rpp in only_recipe:
#                             m.settings.add(rpp)
#                     m.save()
#             elif tag_count == 2:
#                 # если это просто товар с разными вариациями или напитки с разным молоком и эспрессо
#                 # добавляем для каждого продукта настройку, и если это рецепт, то он is_default = True
#                 first = ingredients[i_keys[0]]
#                 second = ingredients[i_keys[1]]
#
#                 for f in first:
#                     for s in second:
#                         m = Modification.objects.create(item=i)
#                         m.settings.add(f)
#                         m.settings.add(s)
#                         if only_recipe:
#                             for rpp in only_recipe:
#                                 m.settings.add(rpp)
#                         m.save()
#
#         print("Recording unique rows to 'modification' table...")
#         for c_item in ConfiguredItem.objects.all():
#             m = Modification.objects.filter(item=c_item.item)
#
#             for cs in c_item.settings.all():
#                 m = m.filter(settings__item_recipe__product_tag_id=cs.tag_id, settings__product_id=cs.product_id)
#
#             if not m:
#                 print("Modification didn't found. Exiting...")
#
#             m = m.first()
#
#             for order in c_item.order_set.all():
#                 OrderContent.objects.create(
#                     order=order,
#                     modification=m,
#                     count=c_item.count
#                 )
#
#         print("All done. time = " + str(time.time() - tm))
