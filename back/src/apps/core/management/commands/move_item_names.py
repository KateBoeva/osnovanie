from django.core.management.base import BaseCommand

from src.apps.core.models import Item


class Command(BaseCommand):
    def handle(self, *args, **options):
        for item in Item.objects.all():
            item.name1 = item.name
            item.save()

        print("all done.")
