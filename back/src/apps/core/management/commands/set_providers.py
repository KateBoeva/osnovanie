# from django.core.management.base import BaseCommand
#
# from src.apps.core.models import Expense, ProductProvider
#
#
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         for expense in Expense.objects.all():
#             if expense.provider:
#                 provider = ProductProvider.objects.filter(name__icontains=expense.provider).first() or \
#                            ProductProvider.objects.create(name=expense.provider)
#                 expense.product_provider = provider
#                 expense.save()
#         print("all done.")
