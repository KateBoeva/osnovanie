# Generated by Django 4.1.2 on 2023-01-01 22:00

import datetime
from django.db import migrations, models
import src.apps.common.enums.expense_type_enum


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_alter_basket_created_at_alter_order_created_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='expense',
            name='type',
            field=models.IntegerField(choices=[(1, 'Расходники'), (2, 'Сырьё')], default=src.apps.common.enums.expense_type_enum.ExpenseType['PRODUCTS'], verbose_name='Тип'),
        ),
        migrations.AlterField(
            model_name='basket',
            name='created_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2023, 1, 2, 1, 0, 17, 762529), null=True, verbose_name='Дата создания'),
        ),
        migrations.AlterField(
            model_name='order',
            name='created_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2023, 1, 1, 22, 0, 17, 763246, tzinfo=datetime.timezone.utc), null=True, verbose_name='Дата создания'),
        ),
    ]
