# Generated by Django 4.1.2 on 2023-01-01 22:26

from django.db import migrations, models
import src.apps.common.enums.action_enum


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_alter_basket_created_at_alter_order_created_at'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500, verbose_name='Название')),
                ('volume', models.FloatField(blank=True, null=True, verbose_name='Объём')),
                ('cost', models.FloatField(blank=True, null=True, verbose_name='Общая стоимость')),
                ('action', models.IntegerField(choices=[(1, 'Списание'), (2, 'Добавление')], default=src.apps.common.enums.action_enum.Action['ADDING'], verbose_name='Действие')),
            ],
        ),
    ]
