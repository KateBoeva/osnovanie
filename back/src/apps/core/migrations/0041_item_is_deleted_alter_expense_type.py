# Generated by Django 4.1.2 on 2023-02-06 13:14

from django.db import migrations, models
import src.apps.common.enums.expense_type_enum


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0040_alter_expense_created_at_alter_expense_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='is_deleted',
            field=models.BooleanField(default=False, verbose_name='Удалён ли товар'),
        ),
        migrations.AlterField(
            model_name='expense',
            name='type',
            field=models.IntegerField(choices=[(2, 'Сырьё'), (1, 'Расходники'), (3, 'Служебка')], default=src.apps.common.enums.expense_type_enum.ExpenseType['PRODUCTS'], verbose_name='Тип'),
        ),
    ]
