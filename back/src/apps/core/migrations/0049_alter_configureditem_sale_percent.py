# Generated by Django 4.1.2 on 2023-02-14 07:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0048_remove_configureditem_is_free_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configureditem',
            name='sale_percent',
            field=models.FloatField(default=0, verbose_name='Процент скидки'),
        ),
    ]
