# Generated by Django 4.1.2 on 2023-03-12 11:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0063_alter_expense_options_delayedsupplyitem'),
    ]

    operations = [
        migrations.CreateModel(
            name='Modification',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.IntegerField(default=1, verbose_name='Количество данных позиций в заказе')),
                ('sale_percent', models.FloatField(default=0, verbose_name='Процент скидки')),
                ('item', models.ForeignKey(db_column='item_id', on_delete=django.db.models.deletion.CASCADE, related_name='modifications', to='core.item', verbose_name='Товар')),
                ('settings', models.ManyToManyField(blank=True, db_table='modification_settings', related_name='modifications', to='core.setting', verbose_name='Настройки')),
            ],
            options={
                'verbose_name': 'Модификация товара',
                'verbose_name_plural': 'Модификации товаров',
                'db_table': 'modification',
                'ordering': ['id'],
            },
        ),
    ]
