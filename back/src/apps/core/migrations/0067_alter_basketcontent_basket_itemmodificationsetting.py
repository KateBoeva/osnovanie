# Generated by Django 4.1.2 on 2023-03-14 18:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0066_remove_modification_count_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basketcontent',
            name='basket',
            field=models.ForeignKey(db_column='basket_id', on_delete=django.db.models.deletion.CASCADE, related_name='basket_content', to='core.basket', verbose_name='Заказ'),
        ),
        migrations.CreateModel(
            name='ItemModificationSetting',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_recipe_setting', models.ForeignKey(db_column='item_recipe_setting_id', on_delete=django.db.models.deletion.CASCADE, related_name='settings', to='core.itemrecipesetting', verbose_name='Рецепт товара')),
                ('modification', models.ForeignKey(db_column='modification_id', on_delete=django.db.models.deletion.CASCADE, related_name='settings', to='core.modification', verbose_name='Модификация товара')),
            ],
            options={
                'verbose_name': 'Настройка модификации товара',
                'verbose_name_plural': 'Настройки модификаций товаров',
                'db_table': 'item_modification_setting',
                'ordering': ['id'],
            },
        ),
    ]
