# Generated by Django 4.1.2 on 2023-03-18 13:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0072_rename_itemrecipesetting_recipepositionproduct_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='RecipePosition',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.FloatField(verbose_name='Количество')),
                ('default_product', models.ForeignKey(blank=True, db_column='product_id', null=True, on_delete=django.db.models.deletion.CASCADE, to='core.product', verbose_name='Продукт')),
                ('item', models.ForeignKey(db_column='item_id', on_delete=django.db.models.deletion.CASCADE, related_name='recipe_positions', to='core.item', verbose_name='Товар')),
                ('product_tag', models.ForeignKey(blank=True, db_column='tag_id', null=True, on_delete=django.db.models.deletion.CASCADE, to='core.recipeproducttag', verbose_name='Тэг')),
            ],
            options={
                'verbose_name': 'Ингредиент рецепта',
                'verbose_name_plural': 'Ингредиенты рецептов',
                'db_table': 'recipe_position',
                'ordering': ['id'],
            },
        ),
    ]
