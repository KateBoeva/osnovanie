# Generated by Django 4.1.2 on 2023-05-28 19:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0096_item_name_new_item_name_prefix'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='name1',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Название (часть 1)'),
        ),
        migrations.AddField(
            model_name='item',
            name='name2',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Название (часть 2)'),
        ),
        migrations.AlterUniqueTogether(
            name='item',
            unique_together={('name1', 'name2')},
        ),
        migrations.RemoveField(
            model_name='item',
            name='name_new',
        ),
        migrations.RemoveField(
            model_name='item',
            name='name_prefix',
        ),
    ]
