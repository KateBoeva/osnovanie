from django.db import models


class Basket(models.Model):
    user = models.ForeignKey(
        to='auth.User',
        related_name="baskets",
        verbose_name="Пользователь",
        db_column="user_id",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    created_at = models.DateTimeField(
        verbose_name="Дата создания",
        auto_now_add=True,
        blank=True,
        null=True
    )

    is_sent = models.BooleanField(
        verbose_name='Отправлено ли на кассу',
        default=False
    )

    guest = models.ForeignKey(
        to='core.Guest',
        related_name="baskets",
        verbose_name="Гость",
        db_column="guest_id",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "Корзина"
        verbose_name_plural = "Корзины"
        db_table = "basket"
        ordering = ['id']
