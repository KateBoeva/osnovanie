from django.db import models


class BasketContent(models.Model):
    basket = models.ForeignKey(
        to='core.Basket',
        related_name="basket_content",
        verbose_name="Заказ",
        db_column="basket_id",
        on_delete=models.CASCADE
    )

    modification = models.ForeignKey(
        to='core.Modification',
        related_name="basket_content",
        verbose_name="Модификация",
        db_column="modification_id",
        on_delete=models.CASCADE
    )

    count = models.IntegerField(
        verbose_name="Количество данных позиций в корзине",
        default=1
    )

    sale_percent = models.FloatField(
        verbose_name="Процент скидки",
        default=0
    )

    @property
    def actual_price(self):
        # стоимость товара + стоимость добавок (если есть) + учет скидки (если есть)
        cost = self.modification.item.cost + sum(i.extra_cost for i in self.modification.settings.all())
        return int(cost - cost * self.sale_percent / 100)

    def __str__(self):
        return "{} - {} шт. (Корзина №{})".format(self.modification.item.name, self.count, self.id)

    class Meta:
        verbose_name = "Позиция корзины"
        verbose_name_plural = "Позиции корзин"
        db_table = "basket_content"
        ordering = ['id']
