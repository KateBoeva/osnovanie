from django.db import models


class DelayedSupplyItem(models.Model):
    product = models.ForeignKey(
        to='core.Product',
        related_name="delayed_supply_items",
        verbose_name="Продукт поставки",
        db_column="product_id",
        on_delete=models.CASCADE
    )

    volume = models.FloatField(
        verbose_name="Объём"
    )

    cost = models.FloatField(
        verbose_name="Стоимость"
    )

    expense = models.ForeignKey(
        to='core.Expense',
        related_name="delayed_supply_items",
        verbose_name="Поставка",
        db_column="expense_id",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "Поставка №{} ({})".format(self.expense_id, self.product.name)

    class Meta:
        verbose_name = "Поставка: отложенная"
        verbose_name_plural = "Поставки: отложенные"
        db_table = "delayed_supply_item"
        ordering = ['id']
