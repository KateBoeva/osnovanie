from django.db import models

from src.apps.common.enums.expense_type_enum import ExpenseType


class Expense(models.Model):
    cost = models.FloatField(
        verbose_name='Сумма',
        blank=True,
        null=True
    )

    description = models.TextField(
        verbose_name="Комментарий",
        blank=True,
        null=True
    )

    type = models.IntegerField(
        verbose_name='Тип',
        choices=ExpenseType.choices(),
        default=ExpenseType.PRODUCTS
    )

    delivery_cost = models.IntegerField(
        verbose_name='Стоимость доставки',
        default=0,
        blank=True,
        null=True
    )

    payment_date = models.DateField(
        verbose_name="Дата оплаты",
        blank=True,
        null=True
    )

    getting_date = models.DateField(
        verbose_name="Дата получения",
        blank=True,
        null=True
    )

    product_provider = models.ForeignKey(
        to='core.ProductProvider',
        related_name="expenses",
        verbose_name="Поставщик",
        db_column="product_provider_id",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    def __str__(self):
        return "{} - {} ({}₽)".format(self.payment_date.strftime("%d.%m.%Y %H:%M"), self.description, int(self.cost))

    class Meta:
        verbose_name = "Поставка"
        verbose_name_plural = "Поставки"
        db_table = "expense"
        ordering = ['-getting_date']
