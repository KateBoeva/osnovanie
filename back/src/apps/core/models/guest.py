from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from src.apps.common.enums.loyalty_type_enum import LoyaltyType


class Guest(models.Model):
    name = models.CharField(
        verbose_name="Имя",
        max_length=500
    )

    phone = PhoneNumberField(
        verbose_name="Телефон",
        max_length=12,
        unique=True
    )

    created_at = models.DateTimeField(
        verbose_name="Дата и время регистрации",
        auto_now_add=True,
        blank=True,
        null=True
    )

    loyalty_type = models.IntegerField(
        verbose_name='Тип системы лояльности',
        choices=LoyaltyType.choices(),
        default=LoyaltyType.CASHBACK
    )

    percent = models.IntegerField(
        verbose_name='Процент',
    )

    balance = models.IntegerField(
        verbose_name='Баланс',
        default=0
    )

    def __str__(self):
        return "{} ({})".format(self.phone, self.name)

    class Meta:
        verbose_name = "Гость"
        verbose_name_plural = "Гости"
        db_table = "guest"
        ordering = ['id']
