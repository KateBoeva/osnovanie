from django.db import models


class Item(models.Model):
    name1 = models.CharField(
        verbose_name="Название (часть 1)",
        max_length=500
    )

    name2 = models.CharField(
        verbose_name="Название (часть 2)",
        max_length=500,
        blank=True,
        null=True
    )

    description = models.TextField(
        verbose_name="Описание",
        blank=True,
        null=True
    )

    item_category = models.ForeignKey(
        to='core.ItemCategory',
        related_name="items",
        verbose_name="Категория",
        db_column="item_category_id",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    cost = models.IntegerField(
        verbose_name="Цена",
        blank=True,
        null=True
    )

    self_price = models.FloatField(
        verbose_name="Себестоимость товара",
        blank=True,
        null=True
    )

    is_hidden = models.BooleanField(
        verbose_name="Скрыт ли товар",
        default=False
    )

    is_deleted = models.BooleanField(
        verbose_name="Удалён ли товар",
        default=False
    )

    need_show_default_option_value = models.BooleanField(
        verbose_name="Нужно ли показывать опцию по умолчанию",
        default=False
    )

    @property
    def name(self):
        return str(self.name1) + ((' ' + self.name2) if self.name2 else '')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"
        db_table = "item"
        ordering = ['name1']
        unique_together = ('name1', 'name2',)
