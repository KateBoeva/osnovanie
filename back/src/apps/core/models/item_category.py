from colorfield.fields import ColorField
from django.db import models


class ItemCategory(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=500
    )

    color = ColorField(
        verbose_name='Цвет',
        default='#FFFFFF'
    )

    priority = models.IntegerField(
        verbose_name="Очередность",
        default=0
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Товар: категория"
        verbose_name_plural = "Товары: категории"
        db_table = "item_category"
        ordering = ['priority']
