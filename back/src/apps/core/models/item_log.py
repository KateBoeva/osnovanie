from django.db import models
from django.utils import timezone


class ItemLog(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=500
    )

    cost = models.FloatField(
        verbose_name="Общая стоимость",
        blank=True,
        null=True
    )

    created_at = models.DateTimeField(
        verbose_name="Дата и время списания",
        auto_now_add=True,
        blank=True,
        null=True
    )

    user = models.ForeignKey(
        to='auth.User',
        related_name="item_logs",
        verbose_name="Пользователь",
        db_column="user_id",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name + ' от ' + timezone.localtime(self.created_at).strftime("%d.%m.%y %H:%M")

    class Meta:
        db_table = 'item_log'
        verbose_name = "Товар: история действий"
        verbose_name_plural = "Товары: история действий"
        ordering = ['-id']
