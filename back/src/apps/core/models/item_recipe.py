from django.db import models


class ItemRecipe(models.Model):
    product_tag = models.ForeignKey(
        to='core.RecipeProductTag',
        related_name="tags",
        verbose_name="Тэг",
        db_column="tag_id",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    item = models.ForeignKey(
        to='core.Item',
        related_name="recipes",
        verbose_name="Товар",
        db_column="item_id",
        on_delete=models.CASCADE
    )

    count = models.FloatField(
        verbose_name="Количество",
    )

    @property
    def default_product(self):
        for setting in self.item_recipe_settings.all():
            if setting.is_default:
                return setting.product
        return self.item_recipe_settings.first().product if self.item_recipe_settings.exists() else None

    def __str__(self):
        result = f'Рецепт для товара "{self.item.name}"'
        if self.default_product:
            result += f' ({self.default_product.name})'
        return result

    class Meta:
        verbose_name = "Товар: рецепт"
        verbose_name_plural = "Товары: рецепты"
        db_table = "item_recipe"
        ordering = ['id']
