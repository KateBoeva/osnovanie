from django.db import models


class Measure(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=500
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Единица измерения"
        verbose_name_plural = "Единицы измерения"
        db_table = "measure"
        ordering = ['id']
