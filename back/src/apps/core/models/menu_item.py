from django.db import models


class MenuItem(models.Model):
    item = models.ForeignKey(
        to='core.Item',
        related_name="menu_items",
        verbose_name="Товар",
        db_column="item_id",
        on_delete=models.CASCADE
    )

    name = models.CharField(
        verbose_name="Наименование позиции в меню",
        max_length=255,
        blank=True,
        null=True
    )

    weight = models.IntegerField(
        verbose_name="Вес товара",
        blank=True,
        null=True
    )

    measure = models.ForeignKey(
        to='core.Measure',
        related_name="menu_items",
        verbose_name="Единица измерения",
        db_column="measure_id",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    img = models.ImageField(
        verbose_name="Изображение",
        upload_to='static/img/menu',
        blank=True,
        null=True
    )

    order_num = models.IntegerField(
        verbose_name="Очерёдность"
    )

    is_hidden = models.BooleanField(
        verbose_name="Скрыт ли товар в меню",
        default=False
    )

    def __str__(self):
        return self.item.name

    class Meta:
        verbose_name = "Позиция в меню"
        verbose_name_plural = "Позиции в меню"
        db_table = "menu_item"
        ordering = ['id']
