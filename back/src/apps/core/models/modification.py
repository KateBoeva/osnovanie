from django.db import models
from django.db.models import Q


class Modification(models.Model):
    item = models.ForeignKey(
        to='core.Item',
        related_name="modifications",
        verbose_name="Товар",
        db_column="item_id",
        on_delete=models.CASCADE
    )

    settings = models.ManyToManyField(
        to='core.RecipePositionProduct',
        related_name='modifications',
        verbose_name='Настройки',
        db_table="item_modification_setting",
        blank=True
    )

    @property
    def shown_settings(self):
        sets = []
        for s in self.settings.filter(item_recipe__product_tag__isnull=False).filter(Q(is_default=False) | Q(item_recipe__item__need_show_default_option_value=True)):
            sets += [s.product.short_name]

        return sets

    def __str__(self):
        extra = '' if not self.shown_settings else '(' + ', '.join(self.shown_settings) + ')'
        return '{} {}'.format(self.item.name, extra)

    class Meta:
        verbose_name = "Товар: модификация"
        verbose_name_plural = "Товары: модификации"
        db_table = "modification"
        ordering = ['id']
