from django.db import models
from django.utils import timezone


class OpenOrder(models.Model):
    order = models.ForeignKey(
        to='core.Order',
        related_name='open_orders',
        verbose_name="Заказ",
        db_column="order_id",
        on_delete=models.CASCADE
    )

    item = models.CharField(
        max_length=1024,
        verbose_name='Позиция'
    )

    is_closed = models.BooleanField(
        verbose_name="Закрыта ли позиция",
        default=False
    )

    def __str__(self):
        return timezone.localtime(self.order.created_at).strftime("%H:%M %d.%m.%y")

    class Meta:
        verbose_name = "Заказ: открытый"
        verbose_name_plural = "Заказы: открытые"
        db_table = "open_order"
        ordering = ['-id']
