from django.db import models
from django.utils import timezone

from src.apps.common.enums.status_enum import Status


class Order(models.Model):
    total = models.FloatField(
        verbose_name='Стоимость',
        blank=True,
        null=True
    )

    status = models.IntegerField(
        verbose_name='Статус',
        choices=Status.choices(),
        default=Status.PAID
    )

    user = models.ForeignKey(
        to='auth.User',
        related_name="orders",
        verbose_name="Пользователь",
        db_column="user_id",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    comment = models.CharField(
        verbose_name="Комментарий",
        max_length=500,
        blank=True,
        null=True
    )

    created_at = models.DateTimeField(
        verbose_name="Дата создания",
        auto_now_add=True,
        blank=True,
        null=True
    )

    closed_at = models.DateTimeField(
        verbose_name="Время отдачи",
        blank=True,
        null=True
    )

    self_price = models.FloatField(
        verbose_name='Себестоимость',
        blank=True,
        null=True
    )

    guest = models.ForeignKey(
        to='core.Guest',
        related_name="orders",
        verbose_name="Гость",
        db_column="guest_id",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    def __str__(self):
        items = ', '.join([oc.modification.item.name for oc in self.order_content.all()])
        return "{} ({})".format(timezone.localtime(self.created_at).strftime("%H:%M %d.%m.%y"), items)

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"
        db_table = "order"
        ordering = ['-id']
