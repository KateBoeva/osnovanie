from django.db import models


class OrderContent(models.Model):
    order = models.ForeignKey(
        to='core.Order',
        related_name="order_content",
        verbose_name="Заказ",
        db_column="order_id",
        on_delete=models.CASCADE
    )

    modification = models.ForeignKey(
        to='core.Modification',
        related_name="order_content",
        verbose_name="Модификация",
        db_column="modification_id",
        on_delete=models.CASCADE
    )

    count = models.IntegerField(
        verbose_name="Количество данных позиций в заказе",
        default=1
    )

    def __str__(self):
        return "{} - {} шт. (Заказ №{})".format(self.modification.item.name, self.count, self.id)

    class Meta:
        verbose_name = "Позиция заказа"
        verbose_name_plural = "Позиции заказов"
        db_table = "order_content"
        ordering = ['id']
