from django.db import models

from src.apps.common.enums.product_type_enum import ProductType


class Product(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=500
    )

    short_name = models.CharField(
        verbose_name="Краткое название",
        max_length=500
    )

    measure = models.ForeignKey(
        to='core.Measure',
        related_name="products",
        verbose_name="Единица измерения",
        db_column="measure_id",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    count = models.FloatField(
        verbose_name="Остаток",
        default=0,
    )

    average_cost = models.FloatField(
        verbose_name="Средняя стоимость за единицу",
        default=0
    )

    type = models.IntegerField(
        verbose_name='Тип продукта',
        choices=ProductType.choices(),
        default=ProductType.PRODUCT
    )

    weight = models.IntegerField(
        verbose_name='Вес одной порции заготовки',
        blank=True,
        null=True
    )

    tag = models.ForeignKey(
        to='core.ProductCategory',
        related_name="products",
        verbose_name="Тэг",
        db_column="tag_id",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    ratio = models.FloatField(
        verbose_name="Коэффициент издержек",
        default=1
    )

    def __str__(self):
        return f"{self.name} ({self.measure})"

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"
        db_table = "product"
        ordering = ['name']
