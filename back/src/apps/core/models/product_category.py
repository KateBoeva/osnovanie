from django.db import models


class ProductCategory(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=500
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Продукт: категория"
        verbose_name_plural = "Продукты: категории"
        db_table = "product_category"
        ordering = ['name']
