from django.db import models
from django.utils import timezone

from src.apps.common.enums.action_enum import Action


class ProductLog(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=500
    )

    volume = models.FloatField(
        verbose_name="Объём",
        blank=True,
        null=True
    )

    cost = models.FloatField(
        verbose_name="Общая стоимость",
        blank=True,
        null=True
    )

    action = models.IntegerField(
        verbose_name='Действие',
        choices=Action.choices(),
        blank=True,
        null=True
    )

    reason = models.TextField(
        verbose_name="Причина списания",
        blank=True,
        null=True
    )

    created_at = models.DateTimeField(
        verbose_name="Дата и время действия",
        auto_now_add=True,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name + (' ({})'.format(self.reason) if self.reason else '') + ' от ' + timezone.localtime(self.created_at).strftime("%d.%m.%y %H:%M")

    class Meta:
        db_table = 'product_log'
        verbose_name = "Продукт: история действий"
        verbose_name_plural = "Продукты: история действий"
        ordering = ['-id']
