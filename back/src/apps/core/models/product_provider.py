from django.db import models


class ProductProvider(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=500
    )

    full_name = models.CharField(
        verbose_name="Полное название организации",
        max_length=1024,
        blank=True,
        null=True
    )

    legal_address = models.CharField(
        verbose_name="Юридический адрес организации",
        max_length=1024,
        blank=True,
        null=True
    )

    inn = models.CharField(
        verbose_name="ИНН",
        max_length=32,
        blank=True,
        null=True
    )

    kpp = models.CharField(
        verbose_name="КПП",
        max_length=32,
        blank=True,
        null=True
    )

    ogrn = models.CharField(
        verbose_name="ОГРН",
        max_length=32,
        blank=True,
        null=True
    )

    account = models.CharField(
        verbose_name="Расчетный счет",
        max_length=32,
        blank=True,
        null=True
    )

    bank_name = models.CharField(
        verbose_name="Наименование банка",
        max_length=512,
        blank=True,
        null=True
    )

    bank_inn = models.CharField(
        verbose_name="ИНН банка",
        max_length=32,
        blank=True,
        null=True
    )

    bank_bik = models.CharField(
        verbose_name="БИК банка",
        max_length=32,
        blank=True,
        null=True
    )

    bank_account = models.CharField(
        verbose_name="Корреспондентский счет банка",
        max_length=64,
        blank=True,
        null=True
    )

    bank_legal_address = models.CharField(
        verbose_name="Юридический адрес банка",
        max_length=1024,
        blank=True,
        null=True
    )

    contact_name = models.CharField(
        verbose_name="ФИО контактного лица",
        max_length=500,
        blank=True,
        null=True
    )

    contact_phone = models.CharField(
        verbose_name="Телефон контактного лица",
        max_length=50,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Поставщик"
        verbose_name_plural = "Поставщики"
        db_table = "product_provider"
        ordering = ['id']
