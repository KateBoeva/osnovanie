from django.db import models


class ProductRecipe(models.Model):
    product_tag = models.ForeignKey(
        to='core.RecipeProductTag',
        verbose_name="Тэг",
        db_column="tag_id",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    billet_product = models.ForeignKey(
        to='core.Product',
        related_name="product_recipes",
        verbose_name="Заготовка",
        db_column="billet_product_id",
        on_delete=models.CASCADE
    )

    count = models.FloatField(
        verbose_name="Количество",
        blank=True,
        null=True
    )

    default_product = models.ForeignKey(
        to='core.Product',
        verbose_name="Продукт",
        db_column="product_id",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.default_product.name} для товара "{self.billet_product.name}"'

    class Meta:
        verbose_name = "Продукт: рецепт"
        verbose_name_plural = "Продукты: рецепты"
        db_table = "product_recipe"
        ordering = ['id']
