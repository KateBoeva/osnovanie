from django.db import models


class RecipePositionProduct(models.Model):
    item_recipe = models.ForeignKey(
        to='core.ItemRecipe',
        related_name="item_recipe_settings",
        verbose_name="Рецепт товара",
        db_column="item_recipe_id",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    product = models.ForeignKey(
        to='core.Product',
        related_name="item_recipe_settings",
        verbose_name="Продукт склада",
        db_column="product_id",
        on_delete=models.CASCADE
    )

    extra_cost = models.IntegerField(
        verbose_name="Добавочная стоимость к товару",
        default=0
    )

    specific_weight = models.IntegerField(
        verbose_name="Кастомная раммовка",
        blank=True,
        null=True
    )

    is_default = models.BooleanField(
        verbose_name="Является ли данная настройка по умолчанию",
        default=False
    )

    def __str__(self):
        return 'Продукт ингредиента для товара "{}" ({})'.format(self.item_recipe.item.name, self.product.name)

    class Meta:
        verbose_name = "Продукт ингредиента товара"
        verbose_name_plural = "Продукты ингредиентов товаров"
        db_table = "recipe_position_product"
        ordering = ['id']
