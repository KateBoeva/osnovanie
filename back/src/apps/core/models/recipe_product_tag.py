from django.db import models


class RecipeProductTag(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=500
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Рецепт: тэг"
        verbose_name_plural = "Рецепты: тэги"
        db_table = "recipe_product_tag"
        ordering = ['id']
