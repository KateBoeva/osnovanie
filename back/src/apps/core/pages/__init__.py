from .supplies_page import SuppliesPage
from .goods_page import GoodsPage
from .kassa_page import KassaPage
from .kassa_orders_open_page import KassaOrdersOpenPage
from .kassa_orders_close_page import KassaOrdersClosePage
from .orders_history_page import OrdersHistoryPage
from .products_log_page import ProductsLogPage
from .products_page import ProductsPage
