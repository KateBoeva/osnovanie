from collections import defaultdict
from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.common.enums.action_enum import Action
from src.apps.common.enums.product_type_enum import ProductType
from src.apps.core.models import Product, ProductLog
from src.apps.core.serializers.product_log_serializer import ProductLogSerializer
from src.apps.core.serializers.product_serializer import ProductSerializer


class BilletsCreatePage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'billets_create.html'

    def get(self, request):
        if request.user.is_authenticated:
            return Response({
                'logs': ProductLogSerializer(ProductLog.objects.filter(action=Action.BILLET), many=True).data,
                'products': ProductSerializer(Product.objects.filter(type=ProductType.BILLET), many=True).data
            })

        return HttpResponseRedirect(redirect_to='accounts/login')

    def post(self, request):
        if request.user.is_authenticated:
            data = {i: request.POST[i] for i in request.POST if request.POST[i]}
            csrf = data.pop('csrfmiddlewaretoken')

            if not data:
                return Response({})

            output = defaultdict(lambda: {})
            for k, v in data.items():
                prefix, suffix = k.rsplit('_', 1)
                output[prefix][suffix] = float(v)

            for product in output:
                product_item = Product.objects.get(pk=int(output[product]['name']))
                added_billet_weight = output[product]['count'] * product_item.weight
                added_billet_price = 0
                for recipe_item in product_item.product_recipes.all():
                    added_billet_product_count = recipe_item.count * output[product]['count']
                    added_billet_price += recipe_item.default_product.average_cost * added_billet_product_count
                    recipe_item.default_product.count -= added_billet_product_count
                    recipe_item.default_product.save()

                # max(product_item.count, 0) - чтобы не влияли продукты с отрицательной массой
                final_price = max(product_item.count, 0) * product_item.average_cost + added_billet_price
                final_weight = max(product_item.count, 0) + added_billet_weight
                product_item.average_cost = final_price / final_weight
                product_item.count += added_billet_weight

                product_item.save()
                ProductLog.objects.create(
                    name=product_item.name,
                    volume=added_billet_weight,
                    cost=added_billet_weight * product_item.average_cost,
                    action=Action.BILLET
                )

            return Response({})

        return HttpResponseRedirect(redirect_to='accounts/login')
