from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.core.models import ItemCategory, RecipeProductTag, Product
from src.apps.core.serializers.id_name_serializer import IdNameSerializer
from src.apps.core.serializers.item_category_serializer import ItemCategoryWithItemsSerializer
from src.apps.core.serializers.recipe_product_tag_serializer import RecipeProductTagSerializer


class GoodsPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'goods.html'

    def get(self, request):
        if request.user.is_authenticated:
            queryset = ItemCategory.objects.all()
            return Response({
                'categories': ItemCategoryWithItemsSerializer(queryset, many=True).data,
                'recipe_product_tags': RecipeProductTagSerializer(RecipeProductTag.objects.all(), many=True).data,
                'products': IdNameSerializer(Product.objects.all(), many=True).data,
            })

        return HttpResponseRedirect(redirect_to='accounts/login')

    def post(self, request):
        pass
