from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.core.models import ItemLog
from src.apps.core.serializers.item_log_serializer import ItemLogSerializer


class ItemLogPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'item_log_history.html'

    def get(self, request):
        if request.user.is_authenticated:
            queryset = ItemLog.objects.all()
            return Response({
                'logs': ItemLogSerializer(queryset, many=True).data
            })

        return HttpResponseRedirect(redirect_to='accounts/login')

    def post(self, request):
        pass
