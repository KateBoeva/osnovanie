import datetime

from django.db.models import Sum, F
from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.core.models import Order
from src.apps.core.serializers.order_serializer import SimpleOrderSerializer


class KassaOrdersClosePage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'kassa_orders_close.html'

    def get(self, request):
        if request.user.is_authenticated:
            queryset = Order.objects.filter(created_at__date=datetime.date.today())
            total_day = int(queryset.aggregate(total_day=Sum(F('total')))['total_day'] or 0)

            return Response({
                'orders': SimpleOrderSerializer(queryset, many=True).data,
                'total_day': total_day,
                'day_self_price': total_day - int(sum([i.self_price for i in queryset])),
            })

        return HttpResponseRedirect(redirect_to='accounts/login')
