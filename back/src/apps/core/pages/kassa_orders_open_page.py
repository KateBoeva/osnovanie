from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.core.models import Order
from src.apps.core.serializers.order_serializer import SimpleOrderSerializer


class KassaOrdersOpenPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'kassa_orders_open.html'

    def get(self, request):
        return Response({
            'orders': SimpleOrderSerializer(Order.objects.filter(open_orders__isnull=False).distinct('id').order_by('id'), many=True).data
        })

    def post(self, request):
        pass
