from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.common.enums.action_enum import Action
from src.apps.core.models import Item, Basket
from src.apps.core.serializers.cart_basket_serializer import CartBasketSerializer
from src.apps.core.serializers.cart_item_serializer import CartItemSerializer
from src.apps.core.serializers.id_name_serializer import IdNameSerializer
from src.apps.core.serializers.user_serializer import UserSerializer


class KassaPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'kassa.html'

    def get(self, request):
        # достаем только доступные товары, отсортированные по категориям и наименованию
        queryset = Item.objects.filter(is_hidden=False, is_deleted=False).order_by('item_category', 'name1')
        if request.user.is_authenticated:
            if Basket.objects.filter(user=request.user).exists():
                basket = CartBasketSerializer(Basket.objects.filter(user=request.user).first()).data
            else:
                basket = None
            return Response({
                'user': request.user,
                'items': CartItemSerializer(queryset, many=True).data,
                'basket': basket,
                'reasons': Action.reason_list(),
                'users': UserSerializer(User.objects.all(), many=True).data
            })

        return HttpResponseRedirect(redirect_to='accounts/login')
