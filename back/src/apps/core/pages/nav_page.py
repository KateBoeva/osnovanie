from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer


class NavPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'nav_page.html'

    def get(self, request):
        if request.user.is_authenticated:
            return Response({})

        return HttpResponseRedirect(redirect_to='accounts/login')
