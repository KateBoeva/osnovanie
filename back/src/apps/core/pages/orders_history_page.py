import datetime

from django.db.models import Sum, F
from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.common.enums.status_enum import Status
from src.apps.core.models import Order, Modification
from src.apps.core.serializers.modification_serializer import SimpleModificationSerializer
from src.apps.core.serializers.order_serializer import SimpleOrderSerializer


class OrdersHistoryPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'orders_history.html'

    def get(self, request):
        if request.user.is_authenticated:
            if not request.GET.get('date'):
                day = datetime.date.today()
            else:
                day = datetime.datetime.strptime(request.GET.get('date'), "%Y-%m-%d").date()

            queryset = Order.objects.filter(created_at__month=day.month, created_at__year=day.year, status=Status.PAID)
            month_self_price = sum([i.self_price for i in queryset])
            total_month = queryset.aggregate(total_month=Sum(F('total')))['total_month']
            operation_month = len(queryset)

            queryset = queryset.filter(created_at__day=day.day)
            day_self_price = sum([i.self_price for i in queryset])
            total_day = queryset.aggregate(total_day=Sum(F('total')))['total_day']

            total_day = int(total_day or 0)
            total_month = int(total_month or 0)

            return Response({
                'orders': SimpleOrderSerializer(queryset, many=True).data,
                'total_day': total_day,
                'total_month': total_month,
                'form_day': day.isoformat(),
                'operation_day': len(queryset),
                'operation_month': operation_month,
                'month_self_price': total_month - int(month_self_price),
                'day_self_price': total_day - int(day_self_price),
                'modifications': SimpleModificationSerializer(Modification.objects.all(), many=True).data
            })

        return HttpResponseRedirect(redirect_to='accounts/login')
