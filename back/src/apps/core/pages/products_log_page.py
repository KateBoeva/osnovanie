from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.common.enums.action_enum import Action
from src.apps.core.models import Product, ProductLog
from src.apps.core.serializers.id_name_serializer import IdNameSerializer
from src.apps.core.services import ProductLogService


class ProductsLogPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'products_log_history.html'

    def get(self, request):
        if request.user.is_authenticated:
            item = int(request.GET.get('item', -1))
            action_num = int(request.GET.get('action', -1))

            filter_data = ProductLogService.get_filter_data(item, action_num)
            queryset = ProductLog.objects.filter(**filter_data).exclude(action__in=[Action.ADDING, Action.SELLING, Action.BILLET])

            response_data = ProductLogService.get_page_info(queryset, int(request.GET.get('page', 1)))
            response_data.update({
                'item': item if item >= 0 else None,
                'items': IdNameSerializer(Product.objects.all(), many=True).data,
                'action': action_num if action_num >= 0 else None,
                'actions': Action.withdrawals_dict()
            })

            return Response(response_data)

        return HttpResponseRedirect(redirect_to='accounts/login')

    def post(self, request):
        pass
