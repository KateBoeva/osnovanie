import json

from django.http import HttpResponseRedirect

from src.apps.common.enums.action_enum import Action
from src.apps.core.models import Item, Product, ProductLog, RecipeProductTag, Measure, ProductCategory, ItemLog, \
    RecipePositionProduct

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.core.serializers.id_name_serializer import IdNameSerializer
from src.apps.core.serializers.product_serializer import ProductSerializer
from src.apps.core.serializers.recipe_product_tag_serializer import RecipeProductTagSerializer


class ProductsPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'products.html'

    def get(self, request):
        if request.user.is_authenticated:
            cat_num = int(request.GET.get('category', -1))
            products = Product.objects.all()
            if cat_num >= 0:
                products = products.filter(tag_id=cat_num)

            return Response({
                'products': ProductSerializer(products, many=True).data,
                'items': IdNameSerializer(Item.objects.all(), many=True).data,
                'reasons': Action.product_reason_list(),
                'categories': IdNameSerializer(ProductCategory.objects.all(), many=True).data,
                'recipe_product_tags': RecipeProductTagSerializer(RecipeProductTag.objects.all(), many=True).data,
                'measures': IdNameSerializer(Measure.objects.all(), many=True).data,
                'category': cat_num
            })

        return HttpResponseRedirect(redirect_to='accounts/login')

    def post(self, request):
        if request.user.is_authenticated:
            # if request.POST['action'] == "add":
            #     product = Product.objects.get(pk=request.POST['product_id'])
            #     m = product.count * product.average_cost + float(request.POST['product_cost'])
            #     p = float(request.POST['product_volume']) * product.ratio + product.count
            #     product.average_cost = m / p
            #     product.count = p
            #     product.save()
            #     ProductLog.objects.create(name=product.name, volume=float(request.POST['product_volume']), cost=float(request.POST['product_cost']), action=Action.ADDING)
            if request.POST['action'] == "withdrawal_item":
                item = Item.objects.get(pk=request.POST['item_id'])
                reason = int(request.POST['item_reason'])
                settings = {int(s['tag_id']): int(s['product_id']) for s in json.loads(request.POST.get('settings'))}

                for recipe in item.recipes.all():
                    if recipe.product_tag_id in settings.keys():
                        product = Product.objects.get(pk=settings[recipe.product_tag_id])
                        rpp = RecipePositionProduct.objects.filter(item_recipe=recipe, product=product).first()
                        count = rpp.specific_weight or recipe.count
                    else:
                        product = recipe.default_product
                        count = recipe.count

                    product.count -= count
                    product.save()
                    ProductLog.objects.create(
                        name=product.name,
                        volume=count,
                        cost=count * product.average_cost,
                        action=reason
                    )

                if reason == Action.STAFF:
                    ItemLog.objects.create(
                        name=item.name,
                        cost=sum([i.count * i.default_product.average_cost for i in item.recipes.all()]),
                        user_id=request.POST.get('staff_user', None)
                    )
            else:
                # при списании продукта на складе
                product = Product.objects.get(pk=request.POST['product_id'])
                product.count -= float(request.POST['product_volume'])
                product.save()
                ProductLog.objects.create(
                    name=product.name,
                    volume=float(request.POST['product_volume']),
                    cost=float(request.POST['product_volume']) * product.average_cost,
                    action=int(request.POST.get('product_reason', None)),
                )

            return HttpResponseRedirect(redirect_to='/products')

        return HttpResponseRedirect(redirect_to='accounts/login')
