from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.common.enums.action_enum import Action
from src.apps.core.models import Product, ProductLog, ProductCategory
from src.apps.core.serializers.product_category_serializer import RecipeProductCategoryProductsSerializer
from src.apps.core.serializers.product_serializer import ProductSerializer


class RevisionCreatePage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'revision_create.html'

    def get(self, request):
        if request.user.is_authenticated:
            return Response({
                'products': ProductSerializer(Product.objects.all(), many=True).data,
                'categories': RecipeProductCategoryProductsSerializer(ProductCategory.objects.all(), many=True).data
            })

        return HttpResponseRedirect(redirect_to='accounts/login')

    def post(self, request):
        data = request.POST

        for item in data:
            if item == 'csrfmiddlewaretoken' or data[item] == '':
                continue

            product = Product.objects.get(pk=int(item))
            diff = float(data[item]) - product.count

            if diff < 0:
                ProductLog.objects.create(
                    name=product.name,
                    volume=abs(diff),
                    cost=product.average_cost * abs(diff),
                    action=Action.REVISION
                )

            product.count = data[item]
            product.save()

        return Response({})
