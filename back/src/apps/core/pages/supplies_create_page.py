from collections import defaultdict

from django.http import HttpResponseRedirect, JsonResponse
from django.utils.dateparse import parse_date

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.common.enums.action_enum import Action
from src.apps.common.enums.expense_type_enum import ExpenseType
from src.apps.core.models import Product, ProductLog, Expense, DelayedSupplyItem, ProductProvider
from src.apps.core.serializers.product_provider_serializer import ProductProviderSerializer
from src.apps.core.serializers.product_serializer import ProductSerializer


class SuppliesCreatePage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'supplies_create.html'

    def get(self, request):
        if request.user.is_authenticated:
            products = ProductSerializer(Product.objects.all(), many=True).data
            return Response({
                'products': products,
                'supply_types': ExpenseType.supply_choices(),
                'providers': ProductProviderSerializer(ProductProvider.objects.all(), many=True).data
            })

        return HttpResponseRedirect(redirect_to='accounts/login')

    def post(self, request):
        if request.user.is_authenticated:
            data = {i: request.POST[i] for i in request.POST if request.POST[i]}
            csrf = data.pop('csrfmiddlewaretoken', None)
            payment_date = data.pop('payment_date', None)
            getting_date = data.pop('getting_date', None)
            delivery_cost = data.pop('delivery_cost', None)
            provider = data.pop('provider', None)
            action = data.pop('action', None)

            # при недостатке информации возвращается ответ с указанием
            if not (payment_date and delivery_cost and data):
                return JsonResponse({"message": "all fields are required"})

            if action == 'raw':
                output = defaultdict(lambda: {})
                for k, v in data.items():
                    prefix, suffix = k.rsplit('_', 1)
                    f = v.replace(',', '.')
                    output[prefix][suffix] = float(f)

                supplies = []
                total = 0

                if getting_date:
                    for product in output:
                        product_item = Product.objects.get(pk=int(output[product]['name']))
                        # max(product_item.count, 0) - чтобы не влияли продукты с отрицательной массой
                        if product_item.count < 0:
                            product_item.average_cost = output[product]['cost'] / output[product]['volume']
                        else:
                            product_item.average_cost = (product_item.count * product_item.average_cost + output[product]['cost']) / (output[product]['volume'] * product_item.ratio + product_item.count)
                        product_item.count = output[product]['volume'] * product_item.ratio + product_item.count
                        supplies += [product_item.name]
                        total += output[product]['cost']
                        product_item.save()
                        ProductLog.objects.create(
                            name=product_item.name,
                            volume=output[product]['volume'],
                            cost=output[product]['cost'],
                            action=Action.ADDING
                        )
                else:
                    for product in output:
                        product_item = Product.objects.get(pk=int(output[product]['name']))
                        supplies += [product_item.name]
                        total += output[product]['cost']

                expense = Expense.objects.create(
                    product_provider_id=provider,
                    payment_date=parse_date(payment_date),
                    getting_date=parse_date(getting_date) if getting_date else None,
                    description="Поставка: " + ", ".join(supplies),
                    delivery_cost=delivery_cost,
                    cost=total,
                    type=ExpenseType.PRODUCTS
                )

                if not getting_date:
                    for product in output:
                        DelayedSupplyItem.objects.create(
                            product_id=int(output[product]['name']),
                            volume=output[product]['volume'],
                            cost=output[product]['cost'],
                            expense=expense
                        )

                return Response({})

            output = defaultdict(lambda: {})
            for k, v in data.items():
                prefix, suffix = k.rsplit('_', 1)
                output[prefix][suffix] = v

            for item in output:
                e_type = item
                Expense.objects.create(
                    product_provider_id=provider,
                    payment_date=parse_date(payment_date),
                    getting_date=parse_date(getting_date) if getting_date else None,
                    delivery_cost=delivery_cost,
                    description="Поставка: " + str(output[item]['description']),
                    cost=output[item]['cost'],
                    type=e_type if e_type != ExpenseType.CUSTOM else ExpenseType.PRODUCTS
                )
            return Response({})

        return HttpResponseRedirect(redirect_to='accounts/login')
