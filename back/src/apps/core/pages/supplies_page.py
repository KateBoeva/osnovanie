from django.http import HttpResponseRedirect
from django.db.models import Sum, F
from django.utils.timezone import now

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer

from src.apps.core.models import Expense
from src.apps.common.enums.expense_type_enum import ExpenseType
from src.apps.core.serializers.expense_serializer import ExpenseSerializer


class SuppliesPage(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'supplies.html'
    months = ['null', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь']

    def get(self, request):
        if request.user.is_authenticated:
            current_datetime = now()
            month = int(request.GET.get('month', None) or current_datetime.month)
            year = int(request.GET.get('year', None) or current_datetime.year)

            # лучше как-то по-другому реализовать
            if month == 13:
                month = 1
                year += 1
            elif month == 0:
                month = 12
                year -= 1

            expenses = Expense.objects.filter(getting_date__year=year, getting_date__month=month)
            open_expenses = Expense.objects.filter(getting_date=None, payment_date__year=year, payment_date__month=month)
            raw_q = expenses.filter(type=ExpenseType.PRODUCTS)
            con_q = expenses.filter(type=ExpenseType.CONSUMABLE)

            total_raw = raw_q.aggregate(total=Sum(F('cost')))['total']
            total_con = con_q.aggregate(total=Sum(F('cost')))['total']

            return Response({
                'open_expenses': ExpenseSerializer(open_expenses, many=True).data or None,
                'expenses': ExpenseSerializer(expenses, many=True).data,
                'total_raw': total_raw or 0,
                'total_con': total_con or 0,
                'month': month,
                'year': year,
                'types': ExpenseType.expenses_choices(),
                'date': "{} {}".format(self.months[month], year)
            })

        return HttpResponseRedirect(redirect_to='accounts/login')
