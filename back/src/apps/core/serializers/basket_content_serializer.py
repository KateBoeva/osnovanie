from rest_framework import serializers

from src.apps.core.models import BasketContent
from src.apps.core.serializers.modification_serializer import ModificationSerializer


class BasketContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = BasketContent
        fields = ['id', 'count', 'sale_percent']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['modification'] = ModificationSerializer(instance.modification).data
        response['cost'] = instance.actual_price
        return response
