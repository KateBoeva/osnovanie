from rest_framework import serializers

from src.apps.core.models import Basket
from src.apps.core.serializers.basket_content_serializer import BasketContentSerializer
from src.apps.core.serializers.guest_serializer import GuestSerializer


class CartBasketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Basket
        fields = ['id', 'is_sent']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['basket_content'] = BasketContentSerializer(instance.basket_content, many=True).data
        response['guest'] = GuestSerializer(instance.guest).data if instance.guest else None
        return response
