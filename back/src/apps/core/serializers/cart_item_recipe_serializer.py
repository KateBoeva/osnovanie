from rest_framework import serializers

from src.apps.core.models import ItemRecipe
from src.apps.core.serializers.cart_settings_serializer import CartSettingsSerializer
from src.apps.core.serializers.id_name_serializer import IdNameSerializer


class CartItemRecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemRecipe
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['tag'] = IdNameSerializer(instance.product_tag).data
        response['settings'] = CartSettingsSerializer(instance.item_recipe_settings, many=True).data
        return response


class SimpleItemRecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemRecipe
        fields = ['id']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['tag'] = IdNameSerializer(instance.product_tag).data
        return response

