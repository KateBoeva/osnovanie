from rest_framework import serializers

from src.apps.core.models import Item
from src.apps.core.serializers.cart_item_recipe_serializer import CartItemRecipeSerializer
from src.apps.core.serializers.item_category_serializer import ItemCategorySerializer


class CartItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ['id', 'name1', 'name2', 'cost', 'description']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['name'] = instance.name
        response['recipes'] = CartItemRecipeSerializer(instance.recipes, many=True).data
        response['category'] = ItemCategorySerializer(instance.item_category).data if instance.item_category else None
        return response
