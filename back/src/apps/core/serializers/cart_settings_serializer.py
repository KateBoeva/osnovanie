from rest_framework import serializers

from src.apps.core.models import RecipePositionProduct, Product


class CartSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipePositionProduct
        fields = ['id', 'extra_cost', 'is_default']

    class _ProductSerializer(serializers.ModelSerializer):
        class Meta:
            model = Product
            fields = ['id', 'short_name', 'name']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['product'] = self._ProductSerializer(instance.product).data
        return response
