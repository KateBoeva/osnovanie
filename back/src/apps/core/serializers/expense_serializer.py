from rest_framework import serializers

from src.apps.common.enums.expense_type_enum import ExpenseType
from src.apps.core.models import Expense


class ExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expense
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['type'] = ExpenseType.dict()[instance.type]
        response['delivery_cost'] = instance.delivery_cost or 0
        response['payment_date'] = instance.payment_date.strftime("%d.%m.%y") if instance.payment_date else None
        response['getting_date'] = instance.getting_date.strftime("%d.%m.%y") if instance.getting_date else None
        return response
