from rest_framework import serializers

from src.apps.core.models import ItemCategory


class ItemCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = '__all__'


class ItemCategoryWithItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = '__all__'

    def to_representation(self, instance):
        from src.apps.core.serializers.item_serializer import ItemSerializer
        response = super().to_representation(instance)
        response['items'] = ItemSerializer(instance.items.filter(is_deleted=False), many=True).data
        return response
