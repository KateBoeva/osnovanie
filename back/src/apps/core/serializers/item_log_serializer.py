from django.utils import timezone
from rest_framework import serializers

from src.apps.core.models import ItemLog


class ItemLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemLog
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['cost'] = round(instance.cost, 2) if instance.cost else 0
        response['created_at'] = timezone.localtime(instance.created_at).strftime("%d.%m.%y %H:%M")
        response['user'] = instance.user.username if instance.user else ''
        return response
