from rest_framework import serializers

from src.apps.core.models import ItemRecipe
from src.apps.core.serializers.product_serializer import ProductSerializer
from src.apps.core.serializers.recipe_position_product_serializer import RecipePositionProductSerializer


class ItemRecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemRecipe
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['default_product'] = ProductSerializer(instance.default_product).data
        response['settings'] = RecipePositionProductSerializer(instance.item_recipe_settings, many=True).data
        return response

