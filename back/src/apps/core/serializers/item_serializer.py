from rest_framework import serializers

from src.apps.core.models import Item
from src.apps.core.serializers.item_category_serializer import ItemCategorySerializer
from src.apps.core.serializers.item_recipe_serializer import ItemRecipeSerializer


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['name'] = instance.name
        response['category'] = ItemCategorySerializer(instance.item_category).data if instance.item_category else None
        response['recipes'] = ItemRecipeSerializer(instance.recipes.all(), many=True).data
        response['tags'] = [i.product_tag.id for i in instance.recipes.all() if i.product_tag]
        response['self_price'] = round(sum([i.count * i.default_product.average_cost for i in instance.recipes.all()]), 2)
        response['percent'] = round(float(response['self_price']) / int(instance.cost) * 100, 2)
        return response
