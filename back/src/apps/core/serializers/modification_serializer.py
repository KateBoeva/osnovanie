from rest_framework import serializers

from src.apps.core.models import Item, Modification
from src.apps.core.serializers.recipe_position_product_serializer import RecipePositionProductSerializer


class SimpleModificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Modification
        fields = ['id']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['name'] = str(instance)
        return response


class ModificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Modification
        fields = "__all__"

    class _ItemSerializer(serializers.ModelSerializer):
        class Meta:
            model = Item
            fields = ['id', 'name', 'need_show_default_option_value']

        def to_representation(self, instance):
            response = super().to_representation(instance)
            response['color'] = instance.item_category.color
            return response

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['item'] = self._ItemSerializer(instance.item).data
        response['settings'] = RecipePositionProductSerializer(instance.settings.all(), many=True).data
        return response
