from rest_framework import serializers

from src.apps.core.models import OpenOrder


class OpenOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = OpenOrder
        fields = '__all__'
