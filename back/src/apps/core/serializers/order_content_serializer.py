from rest_framework import serializers

from src.apps.core.models import OrderContent
from src.apps.core.serializers.modification_serializer import ModificationSerializer


class OrderContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderContent
        fields = ['id', 'count']

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['modification'] = ModificationSerializer(instance.modification).data
        return response
