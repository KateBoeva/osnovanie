from django.db.models import Q
from django.utils import timezone
from rest_framework import serializers

from src.apps.core.models import Order
from src.apps.core.serializers.open_order_serializer import OpenOrderSerializer
from src.apps.core.serializers.order_content_serializer import OrderContentSerializer


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['created_at'] = timezone.localtime(instance.created_at).strftime("%H:%M")
        response['created_at_date'] = timezone.localtime(instance.created_at).strftime("%d %B, %H:%M")
        response['closed_at'] = timezone.localtime(instance.closed_at).strftime("%H:%M") if instance.closed_at else ''
        response['order_content'] = OrderContentSerializer(instance.order_content, many=True).data
        return response


class SimpleOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        tmp = []
        # для всех позиций в заказе отбираем настройки, которые можно менять или нужно обязательно указывать
        for oc in instance.order_content.all():
            settings = oc.modification.settings.filter(item_recipe__product_tag__isnull=False)\
                .filter(Q(is_default=False) | Q(item_recipe__item__need_show_default_option_value=True))
            t = ' (' + ', '.join([i.product.short_name for i in settings]) + ')' if settings else ''
            tmp += [{
                'id': oc.id,
                'name': oc.modification.item.name + t,
                'color': oc.modification.item.item_category.color,
                'count': oc.count
            }]

        response['order_content'] = tmp
        response['open_order'] = OpenOrderSerializer(instance.open_orders.all(), many=True).data
        response['created_at'] = timezone.localtime(instance.created_at).strftime("%H:%M")
        response['closed_at'] = timezone.localtime(instance.closed_at).strftime("%H:%M") if instance.closed_at else ''
        response['comment'] = instance.comment or ''
        return response
