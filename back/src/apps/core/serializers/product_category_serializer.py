from rest_framework import serializers

from src.apps.core.models import ProductCategory


class RecipeProductCategoryProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = '__all__'

    def to_representation(self, instance):
        from src.apps.core.serializers.product_serializer import ProductSerializer
        response = super().to_representation(instance)
        response['products'] = ProductSerializer(instance.products.all(), many=True).data
        return response
