from django.utils import timezone
from rest_framework import serializers

from src.apps.common.enums.action_enum import Action
from src.apps.core.models import ProductLog


class ProductLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductLog
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['action'] = Action.reason_dict()[instance.action]
        response['reason'] = instance.reason or ''
        response['volume'] = round(instance.volume, 2)
        response['cost'] = round(instance.cost, 2) if instance.cost else 0
        response['created_at'] = timezone.localtime(instance.created_at).strftime("%d.%m.%y %H:%M")
        return response
