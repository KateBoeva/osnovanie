from rest_framework import serializers

from src.apps.core.models import ProductProvider


class ProductProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductProvider
        fields = '__all__'
