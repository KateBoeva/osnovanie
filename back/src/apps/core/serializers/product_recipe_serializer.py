from rest_framework import serializers

from src.apps.core.models import ProductRecipe
from src.apps.core.serializers.id_name_serializer import IdNameSerializer


class ProductRecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductRecipe
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['product'] = IdNameSerializer(instance.default_product).data
        # response['product_tag'] = instance.product_tag.name or ''
        response['average_price'] = round(instance.default_product.average_cost * instance.count, 2)
        response['count_measure'] = '{} {}'.format(round(instance.count), instance.default_product.measure.name)
        return response

