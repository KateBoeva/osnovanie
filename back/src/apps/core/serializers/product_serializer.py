from rest_framework import serializers

from src.apps.core.models import Product
from src.apps.core.serializers.id_name_serializer import IdNameSerializer
from src.apps.core.serializers.product_recipe_serializer import ProductRecipeSerializer
from src.apps.core.serializers.recipe_product_tag_serializer import RecipeProductTagSerializer


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

    def to_representation(self, instance):
        from src.apps.common.enums.product_type_enum import ProductType
        response = super().to_representation(instance)
        response['tag'] = RecipeProductTagSerializer(instance.tag).data
        response['count_measure'] = '{} {}'.format(round(instance.count, 1), instance.measure.name)
        response['name_measure'] = '{} ({})'.format(instance.name, instance.measure.name)
        response['measure'] = IdNameSerializer(instance.measure).data
        response['average_cost'] = instance.average_cost
        response['recipe'] = ProductRecipeSerializer(instance.product_recipes.all(), many=True).data

        if instance.type == ProductType.BILLET:
            response['name_weight_measure'] = '{} ({} {})'.format(instance.name, instance.weight, instance.measure.name)

        return response
