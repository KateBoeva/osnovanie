from rest_framework import serializers

from src.apps.core.models import RecipePositionProduct
from src.apps.core.serializers.cart_item_recipe_serializer import SimpleItemRecipeSerializer


class RecipePositionProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipePositionProduct
        fields = '__all__'

    def to_representation(self, instance):
        from src.apps.core.serializers.product_serializer import ProductSerializer

        response = super().to_representation(instance)
        response['product'] = ProductSerializer(instance.product).data
        response['recipe'] = SimpleItemRecipeSerializer(instance.item_recipe).data
        return response
