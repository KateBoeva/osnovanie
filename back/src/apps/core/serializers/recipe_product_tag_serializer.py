from rest_framework import serializers

from src.apps.core.models import RecipeProductTag
from src.apps.core.serializers.recipe_position_product_serializer import RecipePositionProductSerializer


class RecipeProductTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeProductTag
        fields = '__all__'


class RecipeProductTagProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeProductTag
        fields = '__all__'

    def to_representation(self, instance):
        from src.apps.core.serializers.product_serializer import ProductSerializer
        response = super().to_representation(instance)
        response['products'] = ProductSerializer(instance.products.all(), many=True).data
        return response


class AllSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeProductTag
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['settings'] = RecipePositionProductSerializer(instance.settings.all(), many=True).data
        return response
