from django.core.paginator import Paginator

from src.apps.core.models import Product
from src.apps.core.serializers.product_log_serializer import ProductLogSerializer


class ProductLogService:

    @staticmethod
    def get_page_info(queryset, page=1):
        p = Paginator(queryset, 100)
        prev_p = page - 1 if page > 1 else None
        next_p = page + 1 if p.num_pages > page else None

        # link = request.META['PATH_INFO']
        # sym = "&" if len(request.GET) else "?"
        # prev_link = link + sym + "page=" + str(prev_p or "")
        # next_link = link + sym + "page=" + str(next_p or "")

        return {
            'logs': ProductLogSerializer(p.page(page).object_list, many=True).data,
            'page': page,
            'prev': prev_p,
            'next': next_p,
            # 'prev_link': prev_link,
            # 'next_link': next_link,
            'num_pages': p.num_pages
        }

    @staticmethod
    def get_filter_data(item=-1, action_num=-1):
        filter_data = {}
        if item >= 0:
            filter_data.update({'name': Product.objects.get(pk=item).name})

        if action_num >= 0:
            filter_data.update({'action': action_num})

        return filter_data
