from django.urls import path

from rest_framework.routers import SimpleRouter

from src.apps.core.pages import ProductsPage, SuppliesPage, GoodsPage, ProductsLogPage
from src.apps.core.pages.billets_create_page import BilletsCreatePage
from src.apps.core.pages.item_log_page import ItemLogPage
from src.apps.core.pages.kassa_page import KassaPage
from src.apps.core.pages.kassa_orders_close_page import KassaOrdersClosePage
from src.apps.core.pages.kassa_orders_open_page import KassaOrdersOpenPage
from src.apps.core.pages.orders_history_page import OrdersHistoryPage
from src.apps.core.pages.revision_create_page import RevisionCreatePage
from src.apps.core.pages.supplies_create_page import SuppliesCreatePage
from src.apps.core.views.basket_viewset import BasketViewSet
from src.apps.core.views.expense_viewset import ExpenseViewSet
from src.apps.core.views.guest_viewset import GuestViewSet
from src.apps.core.views.item_viewset import ItemViewSet
from src.apps.core.views.order_viewset import OrderViewSet
from src.apps.core.views.recipe_product_tag_viewset import RecipeProductTagViewSet
from src.apps.core.views.product_viewset import ProductViewSet
from src.apps.core.views.recipe_viewset import ItemRecipeViewSet
from src.apps.core.views.setting_vieswet import SettingViewSet

app_name = "core"

router = SimpleRouter()

router.register("recipes", ItemRecipeViewSet, basename="recipes")
router.register("products-api", ProductViewSet, basename="products")
router.register("tags", RecipeProductTagViewSet, basename="tags")
router.register("settings", SettingViewSet, basename="settings")
router.register("items", ItemViewSet, basename="items")
router.register("current_basket", BasketViewSet, basename="basket")
router.register("orders", OrderViewSet, basename="orders")
router.register("expenses", ExpenseViewSet, basename="expenses")
router.register("guests", GuestViewSet, basename="guests")

urlpatterns = [
    path('', OrdersHistoryPage.as_view()),

    path('products', ProductsPage.as_view()),
    path('products_log', ProductsLogPage.as_view()),
    path('staff_log', ItemLogPage.as_view()),

    path('goods', GoodsPage.as_view()),

    path('billets_create', BilletsCreatePage.as_view()),
    path('revision_create', RevisionCreatePage.as_view()),

    path('supplies', SuppliesPage.as_view()),
    path('supplies_create', SuppliesCreatePage.as_view()),

    path('kassa', KassaPage.as_view(), name='kassa'),
    path('kassa_orders_close', KassaOrdersClosePage.as_view()),
    path('kassa_orders_open', KassaOrdersOpenPage.as_view()),
]

urlpatterns += router.urls
