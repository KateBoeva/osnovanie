import json

from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from src.apps.core.models import Basket, Item, BasketContent, Modification
from src.apps.core.serializers.basket_content_serializer import BasketContentSerializer


class BasketViewSet(viewsets.ModelViewSet):
    queryset = Basket.objects.all()
    serializer_class = BasketContentSerializer

    def create(self, request, *args, **kwargs):
        # добавление +-1 существующего товара в корзине
        if "to_add" in request.POST.keys():
            basket_content = BasketContent.objects.get(pk=int(request.POST['ci_id']))
            if int(request.POST['to_add']):
                basket_content.count += 1
            elif basket_content.count > 1:
                basket_content.count -= 1
            basket_content.save()
            return Response("OK", status=status.HTTP_204_NO_CONTENT)

        settings = json.loads(request.POST.get('settings'))
        item = Item.objects.get(pk=int(request.POST.get('item_id')))
        sale_percent = float(request.POST.get('sale_percent'))
        modification = Modification.objects.filter(item=item)

        for setting in settings:
            tag_id = int(setting['tag_id'])
            product_id = int(setting['product_id'])
            modification = modification.filter(settings__item_recipe__product_tag_id=tag_id, settings__product_id=product_id)

        # TODO сделать обработку ошибки, если модификация не найдена
        # TODO сделать генерацию новых модификаций при добавлении новых настроек
        modification = modification.first()

        potential_basket = Basket.objects.filter(user=request.user)
        if potential_basket.exists():
            basket_content = BasketContent.objects.create(
                basket=potential_basket.first(),
                modification=modification,
                sale_percent=sale_percent
            )
            return Response(BasketContentSerializer(basket_content).data, status=status.HTTP_200_OK)

        basket = Basket.objects.create(user=request.user)
        basket_content = BasketContent.objects.create(
            basket=basket,
            modification=modification,
            sale_percent=sale_percent
        )
        return Response(BasketContentSerializer(basket_content).data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['POST'])
    def delete_item(self, request, *args, **kwargs):
        BasketContent.objects.get(pk=int(request.data['pk'])).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=['PATCH'])
    def set_guest(self, request, *args, **kwargs):
        basket = Basket.objects.filter(user=request.user).first()
        if basket:
            basket.guest_id = request.data['pk']
            basket.save()

        return Response(status=status.HTTP_200_OK)

    @action(detail=False, methods=['PATCH'])
    def unset_guest(self, request, *args, **kwargs):
        basket = Basket.objects.filter(user=request.user).first()
        if basket:
            basket.guest_id = None
            basket.save()

        return Response(status=status.HTTP_200_OK)

    @action(detail=False, methods=['PATCH'])
    def set_unsent(self, request, *args, **kwargs):
        current = self.get_queryset().get(user=request.user)
        current.is_sent = False
        current.save()
        return Response(status=status.HTTP_200_OK)
