from rest_framework import viewsets

from src.apps.core.models import ItemCategory
from src.apps.core.serializers.item_category_serializer import ItemCategorySerializer


class ItemCategoryViewSet(viewsets.ModelViewSet):
    queryset = ItemCategory.objects.all()
    serializer_class = ItemCategorySerializer

