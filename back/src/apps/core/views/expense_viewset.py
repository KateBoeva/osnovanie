from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from src.apps.common.enums.action_enum import Action
from src.apps.core.models import Expense, ProductLog
from src.apps.core.serializers.expense_serializer import ExpenseSerializer


class ExpenseViewSet(viewsets.ModelViewSet):
    queryset = Expense.objects.all()
    serializer_class = ExpenseSerializer

    @action(detail=False, methods=['POST'])
    def mark_as_received(self, request, *args, **kwargs):
        expense = Expense.objects.get(pk=int(request.data['pk']))
        expense.getting_date = timezone.localtime(timezone.now()).date()
        expense.save()

        supplies = []
        total = 0
        delayed_items = expense.delayed_supply_items.all()
        for item in delayed_items:
            # max(item.product.count, 0) - чтобы не влияли продукты с отрицательной массой
            final_price = max(item.product.count, 0) * item.product.average_cost + item.cost
            final_weight = item.volume * item.product.ratio + max(item.product.count, 0)
            item.product.average_cost = final_price / final_weight
            item.product.count = final_weight
            supplies += [item.product.name]
            total += item.cost
            item.product.save()
            ProductLog.objects.create(
                name=item.product.name,
                volume=item.volume,
                cost=item.cost,
                action=Action.ADDING
            )

        delayed_items.delete()

        return Response(status=status.HTTP_200_OK)
