from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from src.apps.core.models import Guest
from src.apps.core.serializers.guest_serializer import GuestSerializer


class GuestViewSet(viewsets.ModelViewSet):
    queryset = Guest.objects.all()
    serializer_class = GuestSerializer

    @action(detail=True, methods=['GET'])
    def check_guest(self, request, *args, **kwargs):
        obj = Guest.objects.filter(pk=kwargs['pk'])
        return Response({
            'is_exist': obj.exists(),
            'guest': self.get_serializer(obj.first()).data if obj.exists() else None
        })
