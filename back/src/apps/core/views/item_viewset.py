from collections import defaultdict
from itertools import product

from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from src.apps.core.models import Item, ItemRecipe, RecipePositionProduct, Modification
from src.apps.core.serializers.item_serializer import ItemSerializer


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

    def parse_form(self, request_data):
        item_data = {}
        recipe_data = {}

        for i in request_data:
            if request_data[i]:
                if i.startswith('item'):
                    item_data[i.split('-')[1]] = request_data[i]
                elif i.startswith('recipe-'):
                    recipe_data[i.split('-')[1]] = request_data[i]

        recipes = defaultdict(lambda: {})
        for k, v in recipe_data.items():
            prefix, suffix = k.rsplit('_', 1)
            recipes[suffix][prefix] = v

        for r in recipes:
            recipes[r].update({'settings': {}})

        for i in request_data:
            if request_data[i]:
                if i.startswith('recipe_'):
                    name = i.split('-')
                    recipe_id = name[0].split('_')[1]
                    setting_id = name[1].split('_')[1]
                    if setting_id in recipes[recipe_id]['settings'].keys():
                        recipes[recipe_id]['settings'][setting_id].update({name[2]: request_data[i]})
                    else:
                        recipes[recipe_id]['settings'].update({setting_id: {name[2]: request_data[i]}})

        for i in recipes:
            if not recipes[i]['settings']:
                return Response({"message": "Добавьте к ингредиенту хотя бы один продукт"})

        item_data['is_hidden'] = 'is_hidden' not in item_data.keys()
        if 'is_deleted' in item_data.keys():
            item_data['is_deleted'] = True
        if 'need_show_default_option_value' in item_data.keys():
            item_data['need_show_default_option_value'] = True
        if 'tag' in item_data.keys():
            tag = item_data.pop('tag')
            item_data.update({'item_category': tag})

        return item_data, recipes

    def partial_update(self, request, *args, **kwargs):
        item_data, recipes = self.parse_form(request.POST)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=item_data, partial=True)
        if not serializer.is_valid():
            return Response({"message": "Заполните поля корректно"})

        item = serializer.save()
        sets = {r: [] for r in recipes}
        for r in recipes:
            if 'tag_id' in recipes[r]:
                i_r = ItemRecipe.objects.get(item=item, product_tag_id=recipes[r]['tag_id'])
                settings = recipes[r]['settings']
                for s in settings:
                    rpp = RecipePositionProduct.objects.filter(item_recipe=i_r, product_id=settings[s]['product_id'])
                    if rpp.exists():
                        sets[r] += [rpp.first()]
                    else:
                        sets[r] += [RecipePositionProduct.objects.create(
                            item_recipe=i_r,
                            product_id=settings[s]['product_id'],
                            extra_cost=settings[s]['count']
                        )]

            # else:
            #     i_r = ItemRecipe.objects.get(item=item, product_tag=None)

        for r in recipes:
            if 'tag_id' in recipes[r]:
                i_r = ItemRecipe.objects.get(item=item, product_tag_id=recipes[r]['tag_id'])
                if i_r.count != float(recipes[r]['count']):
                    i_r.count = float(recipes[r]['count'])
                    i_r.save()

                settings = recipes[r]['settings']
                for s in settings:
                    rpp = RecipePositionProduct.objects.get(item_recipe=i_r, product_id=settings[s]['product_id'])
                    is_changed = False
                    if 'count' in settings[s] and rpp.extra_cost != int(settings[s]['count']):
                        rpp.extra_cost = int(settings[s]['count'])
                        is_changed = True

                    is_default = 'is_default' in settings[s].keys()
                    if is_default and rpp.is_default != is_default:
                        rpp.is_default = is_default
                        is_changed = True
                        for i in RecipePositionProduct.objects.filter(item_recipe=i_r).exclude(pk=rpp.pk):
                            i.is_default = False
                            i.save()

                    if is_changed:
                        rpp.save()

        k = list(product(*(sets[i] for i in sets)))
        for i in k:
            modification = Modification.objects.filter(item=item)

            for s in i:
                modification = modification.filter(
                    settings__item_recipe__product_tag=s.item_recipe.product_tag,
                    settings__product=s.product
                )

            if not modification.exists():
                modification = Modification.objects.create(item=item)
                for s in i:
                    modification.settings.add(s)
                modification.save()

        item.self_price = sum([i.count * i.default_product.average_cost for i in item.recipes.all()])
        item.save()

        return Response({"message": 1}, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        item_data, recipes = self.parse_form(request.POST)

        s = self.get_serializer(data=item_data)
        if not s.is_valid():
            return Response({"message": "Заполните поля корректно"})

        item = s.save()

        sets = {}
        for r in recipes:
            settings = recipes[r].pop('settings')
            recipe = ItemRecipe.objects.create(
                product_tag_id=recipes[r]['tag_id'] if 'tag_id' in recipes[r].keys() else None,
                count=recipes[r]['count'],
                item=item
            )

            sets.update({recipe.id: []})
            for s in settings:
                rpp = RecipePositionProduct.objects.create(
                    item_recipe=recipe,
                    product_id=settings[s]['product_id'],
                    extra_cost=int(settings[s]['count']) if 'count' in settings[s] else 0,
                    is_default=('is_default' in settings[s].keys())
                )
                sets[recipe.id] += [rpp]

        k = list(product(*(sets[i] for i in sets)))

        for i in k:
            m = Modification.objects.create(item=item)
            for j in i:
                m.settings.add(j)
            m.save()

        return Response({"message": 1}, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['POST'])
    def change_hiding(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.is_hidden = not obj.is_hidden
        obj.save()
        return Response("OK")

    @action(detail=True, methods=['POST'])
    def mark_as_deleted(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.is_deleted = True
        obj.save()
        return Response("OK")
