import json
from collections import defaultdict

import requests
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from src.apps.common.enums.action_enum import Action
from src.apps.common.enums.status_enum import Status
from src.apps.core.models import Order, Basket, ProductLog, OpenOrder, OrderContent
from src.apps.core.serializers.order_serializer import OrderSerializer, SimpleOrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    http_method_names = ['get', 'post', 'put', 'patch']

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()
        post = dict(request.POST)
        for i in post:
            post[i] = post[i][0]
        data = {i: request.POST[i] for i in request.POST if request.POST[i] and (i.find('_mod') != -1 or i.find('_count') != -1)}
        date = post.pop('date')
        post_copy = post.copy()
        for i in post_copy:
            if i.find('_mod') != -1 or i.find('_count') != -1:
                post.pop(i)
            elif i == 'closed_at':
                post[i] = date + ' ' + post[i]

        output = defaultdict(lambda: {})
        for k, v in data.items():
            prefix, suffix = k.rsplit('_', 1)
            output[prefix][suffix] = int(v)

        content = OrderContent.objects.filter(order_id=kwargs['pk'])

        for i in output:
            oc = content.filter(pk=int(i)).first()
            if oc.modification_id != output[i]['mod']:
                for setting in oc.modification.settings.all():
                    count = (setting.specific_weight or setting.item_recipe.count) * oc.count
                    setting.product.count += count
                    setting.product.save()

                oc.modification_id = output[i]['mod']
                oc.save()

                for setting in oc.modification.settings.all():
                    count = (setting.specific_weight or setting.item_recipe.count) * oc.count
                    setting.product.count -= count
                    setting.product.save()

        serializer = self.get_serializer(instance, data=post, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

    def send_for_pay(self, instance):
        positions = []
        now = timezone.localtime(timezone.now())
        for bc in instance.basket_content.all():
            name = bc.modification.item.name

            m_settings = bc.modification.settings.filter(item_recipe__product_tag__isnull=False)\
                .filter(Q(is_default=False) | Q(item_recipe__item__need_show_default_option_value=True))

            for setting in m_settings:
                name += ' / ' + setting.product.short_name

            positions += [{
                "sku": str(bc.id),
                "editable": True,
                "quantity": bc.count,
                "price": bc.actual_price,
                "tax": 6,
                "text": name,
                "paymentMethodType": 4,
                "paymentSubjectType": 1
            }]

        headers = {
            'x-client-key': 'Application ' + settings.API_KEY,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        data = {
            "id": str(instance.id + 100),
            "number": instance.id + 100,
            "dateTime": str(now),
            "deliveryAddress": "ул. Баумана, д. 9А",
            "device": settings.CASH_NUM,
            "content": {
                "type": 1,
                "positions": positions
            }
        }

        instance.created_at = now
        instance.is_sent = True

        instance.save()
        r = requests.post('https://api.aqsi.ru/pub/v2/Orders/simple', headers=headers, data=json.dumps(data))
        print(r.status_code)

    def create(self, request, *args, **kwargs):
        # вытягиваем текущую корзину
        basket = Basket.objects.filter(user=request.user).first()

        # собираем данные о заказе
        order_place = int(request.POST['order_place'])
        drinks = int(request.POST['drinks'])
        comment = request.POST['comment']

        pre_comment = []
        if order_place:
            pre_comment += ['С собой']
        if drinks:
            pre_comment += ['Напитки с едой']

        full_comment = "[" + ", ".join(pre_comment) + "] " + comment if pre_comment else comment
        # считаем финальную стоимость
        total = sum([element.actual_price * element.count for element in basket.basket_content.all()])

        # создаем заказ и позиции для него
        instance = Order.objects.create(
            total=total,
            user=request.user,
            comment=full_comment,
            created_at=basket.created_at,
            guest=basket.guest
        )

        for bc in basket.basket_content.all():
            oc = OrderContent.objects.create(
                order=instance,
                modification=bc.modification,
                count=bc.count
            )
            instance.order_content.add(oc)
            instance.save()

        # удаляем корзину
        basket.delete()

        # считаем себестоимость заказа
        self_price = 0
        for oc in instance.order_content.all():
            name = oc.modification.item.name
            for setting in oc.modification.settings.all():
                self_price += (setting.specific_weight or setting.item_recipe.count) * oc.count * setting.product.average_cost

            for setting in oc.modification.settings.all():
                name += ' / ' + setting.product.short_name
                count = (setting.specific_weight or setting.item_recipe.count) * oc.count
                setting.product.count -= count
                setting.product.save()
                ProductLog.objects.create(
                    name=setting.product.name,
                    volume=count,
                    cost=setting.product.average_cost * count,
                    action=Action.SELLING,
                    reason=oc.modification.item.name
                )

        instance.self_price = self_price
        instance.save()

        # для всех позиций создаем строки в открытых заказах
        s_data = SimpleOrderSerializer(instance).data
        for oc in s_data['order_content']:
            for n in range(oc['count']):
                OpenOrder.objects.create(
                    order=instance,
                    item=oc['name']
                )

        return Response(instance.id, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['POST'])
    def send_order_for_pay(self, request, *args, **kwargs):
        # отправляем заказ на кассу
        basket = Basket.objects.filter(user=request.user).first()
        self.send_for_pay(basket)
        return Response("OK", status=status.HTTP_200_OK)

    @action(detail=False, methods=['POST'])
    def check_item(self, request, *args, **kwargs):
        open_order_id = request.POST.get('open_order_id')
        open_order = OpenOrder.objects.get(pk=open_order_id)
        open_order.is_closed = True
        open_order.save()

        order_items = OpenOrder.objects.filter(order=open_order.order)
        closed_items = order_items.filter(is_closed=False)
        is_closed = False if closed_items else True

        if is_closed:
            open_order.order.closed_at = timezone.localtime(timezone.now())
            open_order.order.save()
            order_items.delete()

        return Response({'is_closed': is_closed})

    @action(detail=True, methods=['PATCH'])
    def cancel_order(self, request, *args, **kwargs):
        order = self.get_object()
        order.status = Status.CANCELLED
        order.save()

        for oc in order.order_content.all():
            for setting in oc.modification.settings.all():
                setting.product.count += (setting.specific_weight or setting.item_recipe.count)
                setting.product.save()

        return Response("OK", status=status.HTTP_204_NO_CONTENT)
