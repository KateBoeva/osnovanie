from collections import defaultdict

from rest_framework import viewsets, status
from rest_framework.response import Response

from src.apps.common.enums.product_type_enum import ProductType
from src.apps.core.models import Product, ProductRecipe
from src.apps.core.serializers.item_recipe_serializer import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def parse_form(self, request_data):
        product_data = {}
        recipe_data = {}

        for i in request_data:
            if request_data[i]:
                if i.startswith('product'):
                    product_data[i.split('-')[1]] = request_data[i]
                elif i.startswith('recipe'):
                    recipe_data[i.split('-')[1]] = request_data[i]

        recipes = defaultdict(lambda: {})
        for k, v in recipe_data.items():
            prefix, suffix = k.rsplit('_', 1)
            recipes[suffix][prefix] = v

        product_data['type'] = 2 if 'is_billet' in request_data.keys() else 1

        return product_data, recipes

    def create(self, request, *args, **kwargs):
        product_data, recipes = self.parse_form(request.POST)
        serializer = self.get_serializer(data=product_data)
        if not serializer.is_valid():
            return Response('Заполните все поля')

        product = serializer.save()
        p_recipes = []
        if product_data['type'] == 2:
            # сохранять рецепты
            for r in recipes:
                try:
                    pr = ProductRecipe.objects.create(
                        product_tag_id=int(recipes[r]['tag_id']) if 'tag_id' in recipes[r].keys() else None,
                        billet_product=product,
                        count=float(recipes[r]['count']),
                        default_product_id=int(recipes[r]['product_id'])
                    )
                    p_recipes += [pr]
                except:
                    for p in p_recipes:
                        p.delete()
                    return Response('Заполните корректно поля рецепта')

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        product_data, recipes = self.parse_form(request.POST)
        serializer = self.get_serializer(instance, data=product_data, partial=True)

        if not serializer.is_valid():
            return Response({'message': "Заполните поля корректно"})

        product = serializer.save()

        if product.type == ProductType.BILLET:
            product.product_recipes.all().delete()

            for r in recipes:
                rp = ProductRecipe.objects.create(
                    billet_product=product,
                    count=float(recipes[r]['count'].replace(',', '.')),
                    default_product_id=recipes[r]['product_id'],
                )
                product.product_recipes.add(rp)

            product.save()

        return Response(serializer.data)
