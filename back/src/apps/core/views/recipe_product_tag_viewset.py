from rest_framework import viewsets

from src.apps.core.models import RecipeProductTag
from src.apps.core.serializers.recipe_product_tag_serializer import RecipeProductTagSerializer


class RecipeProductTagViewSet(viewsets.ModelViewSet):
    queryset = RecipeProductTag.objects.all()
    serializer_class = RecipeProductTagSerializer

