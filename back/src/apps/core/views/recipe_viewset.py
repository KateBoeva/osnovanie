from rest_framework import status, mixins, viewsets

from src.apps.core.models import ItemRecipe
from src.apps.core.serializers.item_recipe_serializer import ItemRecipeSerializer


class ItemRecipeViewSet(viewsets.ModelViewSet):
    queryset = ItemRecipe.objects.all()
    serializer_class = ItemRecipeSerializer

