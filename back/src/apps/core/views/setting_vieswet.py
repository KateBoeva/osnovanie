from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from src.apps.core.models import RecipePositionProduct
from src.apps.core.serializers.recipe_product_tag_serializer import AllSettingSerializer
from src.apps.core.serializers.recipe_position_product_serializer import RecipePositionProductSerializer


class SettingViewSet(viewsets.ModelViewSet):
    queryset = RecipePositionProduct.objects.all()

    def get_serializer_class(self):
        if self.action == "all_settings":
            return AllSettingSerializer

        return RecipePositionProductSerializer

    @action(detail=False, methods=['GET'])
    def all_settings(self, request, *args, **kwargs):
        return Response(RecipePositionProductSerializer(self.get_queryset(), many=True).data)
