// показывает все элементы на кассе, если нет никакого запроса
function showAll(class_name=null) {
    let cards = $('.col.align-self-stretch');
    for (let i = 0; i < cards.length; i++) {
        $(`#${cards[i].id}`).show()
    }
}

function clearSearchField() {
    $('#search').val('')
    showAll()
}

function search(value) {
    if (value === "") {
        showAll()
    } else {
        let cards = $('.col.align-self-stretch');
        let val = value.toLowerCase()
        for (let i = 0; i < cards.length; i++) {
            let card_id = `#${cards[i].id}`;
            if ($(`${card_id} .card-name`).text().toLowerCase().includes(val)) {
                $(card_id).show()
            } else {
                $(card_id).hide()
            }
        }
    }
}

function recalcTotal() {
    let prices = $('.basket .item_price')
    let count = $('.basket .count')
    let sum = 0
    for (let i = 0; i < prices.length; i++) {
        sum += +prices[i].innerHTML * +count[i].innerHTML
    }

    $('#total').html(sum)
}

function getDataFrom(url) {
    let result = {}
    $.get(url, function (data) {
        for (let i = 0; i < data.length; i++) {
            let item_id = data[i]['id'];
            result[item_id] = data[i];
        }

        return result
    })

    return result
}

function changeItemCount(item_id, span, to_add) {
    // меняем число
    let val = to_add ? +span.html() + 1 : +span.html() - 1
    let data = {
        'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
        'to_add': +to_add,
        'ci_id': item_id
    }
    if (to_add) {
        $.post('current_basket/', data, function (data) {
            span.html(val)
            recalcTotal()
        })
    } else {
        $.post('current_basket/', data, function (data) {
            if (val > 0) {
                span.html(val)
                recalcTotal()
            }
        })
    }
}
